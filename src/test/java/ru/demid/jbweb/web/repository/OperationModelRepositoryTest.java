package ru.demid.jbweb.web.repository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplication.class)
public class OperationModelRepositoryTest {

    @Autowired OperationModelRepository subj;
    @Autowired
    EntityManager em;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void findByHolderIdAndDate_RegularUse() {
        String date = "07-08-2019";
        List<OperationModel> operations = subj.findByHolderIdAndDate(1L, date, date);

        assertNotNull(operations);
        assertEquals(3, operations.size());
        assertTrue(operations.stream().allMatch(o -> o.getDate().equals(date)));
    }
    @Test
    public void findByHolderIdAndDate_NullDate() {
        List<OperationModel> operations = subj.findByHolderIdAndDate(1L, null, null);

        assertNotNull(operations);
        assertTrue(operations.isEmpty());
    }
    @Test
    public void findByHolderIdAndDate_NullHolder() {
        String date = "07-08-2019";
        List<OperationModel> operations = subj.findByHolderIdAndDate(null, date, date);

        assertNotNull(operations);
        assertTrue(operations.isEmpty());
    }

    @Test
    public void findByFilter_RegularUse() {
        String date = "07-08-2019";
        FilterForOperation filter = new FilterForOperation().setStartDate(date)
                                                            .setFinishDate(date)
                                                            .setIdHolder(1L)
                                                            .setListCategory(Collections.singletonList(em.find(Category.class, 1L)));
        List<OperationModel> operations = subj.findByFilter(filter);
        assertNotNull(operations);
        assertEquals(2, operations.size());
        assertTrue(operations.stream().allMatch(o -> o.getDate().equals(date)));
    }

    @Test()
    public void findByFilter_EmptyId() {
        String date = "07-08-2019";
        FilterForOperation filter = new FilterForOperation().setStartDate(date)
                                                            .setFinishDate(date)
                                                            .setListCategory(Collections.singletonList(em.find(Category.class, 1L)));
        thrown.expect(ServiceException.class);
        thrown.expectMessage("Problem with holder ID");
        subj.findByFilter(filter);
    }

    @Test()
    public void findByFilter_WrongId() {
        String date = "07-08-2019";
        FilterForOperation filter = new FilterForOperation().setStartDate(date)
                                                            .setFinishDate(date)
                                                            .setIdHolder(99L)
                                                            .setListCategory(Collections.singletonList(em.find(Category.class, 1L)));
        List<OperationModel> operations = subj.findByFilter(filter);
        assertNotNull(operations);
        assertTrue(operations.isEmpty());
    }

    @Test()
    public void findByFilter_NullDate() {
        FilterForOperation filter = new FilterForOperation().setIdHolder(1L)
                                                            .setListCategory(Collections.singletonList(em.find(Category.class, 1L)));

        thrown.expect(ServiceException.class);
        thrown.expectMessage("Date required");
        subj.findByFilter(filter);
    }
}
