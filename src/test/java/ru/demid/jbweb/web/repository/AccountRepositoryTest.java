package ru.demid.jbweb.web.repository;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.entity.Account;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplication.class)
class AccountRepositoryTest {

    @Autowired AccountRepository subj;

    @Test
    void findAccounts() {
        List<Account> accounts = subj.findAccounts(1L);
        assertNotNull(accounts);
        assertEquals(3, accounts.size());
        assertTrue(accounts.stream().anyMatch(account -> account.getName().equals("Наличка")));
        assertTrue(accounts.stream().allMatch(account -> account.getAccountHolder().getId()==1L));
    }

    @Test
    void findAccounts_WrongId() {
        List<Account> accounts = subj.findAccounts(111L);
        assertNotNull(accounts);
        assertTrue(accounts.isEmpty());
    }

    @Test
    void findAccounts_NullId() {
        List<Account> accounts = subj.findAccounts(null);
        assertNotNull(accounts);
        assertTrue(accounts.isEmpty());
    }
}