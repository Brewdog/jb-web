package ru.demid.jbweb.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.demid.jbweb.web.security.CustomUserDetailsImpl;
import ru.demid.jbweb.web.security.UserGrantedAuthorityImpl;

import static java.util.Collections.singleton;
import static ru.demid.jbweb.web.security.UserRoles.USER;

@Configuration
public class MockSecurityConfiguration {
    @Bean
    public UserDetailsService userDetailsService(){
        return s -> new CustomUserDetailsImpl(
                2L,
                "petrov@gmail.com",
                "123123",
                singleton(new UserGrantedAuthorityImpl(USER))
        );
    }
}
