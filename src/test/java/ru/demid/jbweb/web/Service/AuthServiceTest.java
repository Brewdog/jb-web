package ru.demid.jbweb.web.Service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.demid.jbweb.web.api.converter.Converter;
import ru.demid.jbweb.web.entity.UserModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.UserRepository;
import ru.demid.jbweb.web.security.UserRoles;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;

import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {
    @InjectMocks
    AuthService subj;

    @Mock
    UserRepository repository;
    @Mock
    PasswordEncoder encoder;
    @Mock
    Converter<UserModel, UserDTO> userDtoConverter;

    private UserModel getTestUserModel() {
        UserModel userModel = new UserModel();
        userModel.setId(2);
        userModel.setEmail("ptr@mail.ru");
        userModel.setPassword("password");
        userModel.setName("Petr");
        userModel.setUserRoles(singleton(UserRoles.USER));
        return userModel;
    }

    @Test
    public void auth_userNotFound() {
        when(repository.findByEmail("ptr@mail.ru")).thenReturn(null);

        UserDTO userDTO = subj.auth("ptr@mail.ru");
        assertNull(userDTO);
        verify(repository, times(1)).findByEmail("ptr@mail.ru");

    }

    @Test
    public void auth_userFound() {

        when(repository.findByEmail("ptr@mail.ru")).thenReturn(getTestUserModel());
        when(userDtoConverter.convert(getTestUserModel()))
                .thenReturn(new UserDTO(2,
                        "ptr@mail.ru",
                        "Petr",
                        "Petroff",
                        emptyList()));

        UserDTO user = subj.auth("ptr@mail.ru");
        assertNotNull(user);
        verify(repository, times(1)).findByEmail("ptr@mail.ru");
        verify(userDtoConverter, times(1)).convert(getTestUserModel());

    }

    @Test
    public void registration_RegularUse() {
        when(encoder.encode("pasword")).thenReturn("hex");
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("ptr@mail.ru");
        userDTO.setId(0);
        userDTO.setLastName("Petrovskiy");
        userDTO.setName("Petr");
        UserModel model = new UserModel();
        model.setId(0);
        model.setEmail("ptr@mail.ru");
        model.setPassword("hex");
        model.setName("Petr");
        model.setLastName("Petrovskiy");
        model.setUserRoles(singleton(UserRoles.USER));
        when(userDtoConverter.convert(model)).thenReturn(userDTO);
        when(repository.saveAndFlush(model)).thenReturn(model);

        UserDTO user = subj.registration("ptr@mail.ru", "pasword", "Petr", "Petrovskiy");
        assertNotNull(user);
        assertEquals(userDTO, user);
        verify(encoder, times(1)).encode("pasword");
        verify(repository, times(1)).saveAndFlush(model);
        verify(userDtoConverter, times(1)).convert(model);
    }

    @Test(expected = ServiceException.class)
    public void registration_nullUser() {
        subj.registration(null, null, null, null);
    }

    @Test(expected = ServiceException.class)
    public void registration_withBlankLetterUser() {
        subj.registration("", "", " ", "");
    }

}