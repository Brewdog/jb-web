package ru.demid.jbweb.web.Service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.entity.UserModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.AccountRepository;
import ru.demid.jbweb.web.repository.UserRepository;
import ru.demid.jbweb.web.service.AccountService;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @InjectMocks
    AccountService subj;
    @Mock
    AccountRepository repo;
    @Mock
    UserRepository userRepo;

    @Rule
    public ExpectedException ex = ExpectedException.none();

    private List<Account> getTestAccounts() {
        return singletonList(new Account()
                .setName("Blabla")
                .setId(99)
                .setBalance(10000f)
                .setSentOperations(emptyList())
                .setReceivedOperations(emptyList()));
    }
    @Test
    public void viewAccounts() {
        when(repo.existsById(2L)).thenReturn(true);
        when(repo.findAccounts(2L)).thenReturn(getTestAccounts());

        List<Account> accounts = subj.viewAccounts(2L);
        verify(repo, times(1)).existsById(2L);
        assertEquals(1, accounts.size());
    }

    @Test
    public void viewAccounts_NotExist() {
        when(repo.existsById(2L)).thenReturn(false);
        ex.expect(ServiceException.class);
        subj.viewAccounts(2L);
    }

    @Test
    public void makeAccount() {
        when(userRepo.getOne(2L)).thenReturn(new UserModel());
        when(repo.save(Mockito.any(Account.class))).thenReturn(getTestAccounts().get(0));
        when(repo.existsById(2L)).thenReturn(true);
        when(subj.viewAccounts(2L)).thenReturn(getTestAccounts());

        Account account = subj.makeAccount("Blabla1", 2L);
        verify(repo, times(1)).save(Mockito.any(Account.class));
        assertEquals("Blabla", account.getName());
    }

    @Test
    public void makeAccountNameExist() {
        when(userRepo.getOne(2L)).thenReturn(new UserModel());
        when(repo.save(Mockito.any(Account.class))).thenReturn(getTestAccounts().get(0));
        when(repo.existsById(2L)).thenReturn(true);
        when(subj.viewAccounts(2L)).thenReturn(getTestAccounts());

        ex.expect(ServiceException.class);
        subj.makeAccount("Blabla", 2L);
    }

    @Test
    public void delAccount() {
        Account account = getTestAccounts().get(0);
        UserModel userModel = new UserModel();
        userModel.setId(2L);
        account.setAccountHolder(userModel);
        when(repo.getOne(99L)).thenReturn(account);
        boolean b = subj.delAccount(99L, 2L);
        verify(repo, times(1)).getOne(99L);
        verify(repo, times(1)).delete(account);
        assertTrue(b);
    }

    @Test
    public void delAccount_WrongIdHolder() {
        Account account = getTestAccounts().get(0);
        UserModel userModel = new UserModel();
        userModel.setId(2L);
        account.setAccountHolder(userModel);
        when(repo.getOne(99L)).thenReturn(account);
        boolean b = subj.delAccount(99L, 1L);
        verify(repo, times(1)).getOne(99L);
        verify(repo, times(0)).delete(account);
        assertFalse(b);
    }

    @Test
    public void updateInfo() {
        Account account = getTestAccounts().get(0);
        UserModel userModel = new UserModel();
        userModel.setId(2L);
        account.setAccountHolder(userModel);
        Account account2 = new Account();
        account2.setAccountHolder(userModel);
        account2.setName("NewName");
        account2.setId(97);
        when(repo.getOne(99L)).thenReturn(account);
        when(repo.saveAndFlush(account)).thenReturn(account2);

        Account account1 = subj.updateInfo(2L, 99L, "NewName", 5000L);
        assertEquals(account2, account1);
        verify(repo, times(1)).saveAndFlush(account);
        verify(repo, times(1)).getOne(99L);
    }

    @Test
    public void updateInfo_WrongID() {
        Account account = getTestAccounts().get(0);
        UserModel userModel = new UserModel();
        userModel.setId(2L);
        account.setAccountHolder(userModel);
        when(repo.getOne(99L)).thenReturn(account);

        ex.expect(ServiceException.class);
        subj.updateInfo(1L, 99L, "NewName", 5000L);
    }
}