package ru.demid.jbweb.web.Service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.entity.UserModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.AccountRepository;
import ru.demid.jbweb.web.repository.CategoryRepository;
import ru.demid.jbweb.web.repository.OperationModelRepository;
import ru.demid.jbweb.web.service.OperationService;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class OperationServiceTest {

    @InjectMocks
    OperationService subj;

    @Mock
    OperationModelRepository repository;
    @Mock
    AccountRepository accRepo;
    @Mock
    CategoryRepository catRepo;

    @Test(expected = ServiceException.class)
    public void viewByCategoryAndDate_NotFound() {

        when(repository.findByHolderIdAndDate(1L, "06-08-2019", "08-08-2019")).thenReturn(emptyList());

        subj.viewByCategoryAndDate(1L, "06-08-2019", "08-08-2019");
    }

    @Test
    public void viewByCategoryAndDate_RegularUse() {
        String date = "06-08-2019";
        OperationModel operationModel = new OperationModel();
        operationModel.setSum(5000);
        operationModel.setCategories(singletonList(new Category("Food")));
        List<OperationModel> listOM = singletonList(operationModel);
        when(repository.findByHolderIdAndDate(1L, date, date)).thenReturn(listOM);

         List<OperationModel> list = subj.viewByCategoryAndDate(1, date, date);

        assertNotNull(list);
        verify(repository, times(1)).findByHolderIdAndDate(1L, date, date);
    }

    @Test(expected = ServiceException.class)
    public void viewByCategoryAndDate_CatchSpringException() {
        String date = "06-08-2019";
        when(repository.findByHolderIdAndDate(1L, date, date)).thenThrow(new DataIntegrityViolationException("Cause"));

         subj.viewByCategoryAndDate(1, date, date);
    }

    @Test
    public void addNewOperation(){
        String date = "06-08-2019";
        UserModel user = new UserModel();
        user.setId(1);

        Account account = getTestAccount();

        when(accRepo.getOne(99L)).thenReturn(account);
        OperationModel operationModel = new OperationModel();
        operationModel.setSum(5000);
        operationModel.setDate(date);
        operationModel.setSender(account);
        operationModel.setCategories(singletonList(new Category("Food")));

        subj.addNewOperation(1, 99L, null, 5000, date, null, singletonList(new Category("Food")));
        verify(accRepo, times(1)).getOne(99L);
        verify(repository, times(1)).save(operationModel);
        verifyZeroInteractions(catRepo);

    }

    @Test
    public void addNewOperation_EmptyCategories(){
        String date = "06-08-2019";
        UserModel user = new UserModel();
        user.setId(1);

        Account account = getTestAccount();

        when(accRepo.getOne(99L)).thenReturn(account);
        OperationModel operationModel = new OperationModel();
        operationModel.setSum(5000);
        operationModel.setDate(date);
        operationModel.setSender(account);
        operationModel.setCategories(singletonList(catRepo.getOne(6L)));

        OperationModel operationModel1 = subj.addNewOperation(1, 99L, null, 5000, date, null, emptyList());
        verify(accRepo, times(1)).getOne(99L);
        verify(repository, times(1)).save(operationModel);
        verify(catRepo, times(2)).getOne(6L);
        assertEquals(operationModel, operationModel1);

    }

    @Test(expected = ServiceException.class)
    public void addNewOperation_WrongHolder(){
        String date = "06-08-2019";
        Account account = getTestAccount();

        when(accRepo.getOne(99L)).thenReturn(account);

        subj.addNewOperation(2, 99L, null, 5000, date, null, emptyList());
    }

    @Test(expected = ServiceException.class)
    public void addNewOperation_CatchException(){
        String date = "06-08-2019";

        when(accRepo.getOne(99L)).thenThrow(new DataIntegrityViolationException("Cause"));

        subj.addNewOperation(2, 99L, null, 5000, date, null, emptyList());
    }

    private Account getTestAccount() {
        UserModel user = new UserModel();
        user.setId(1);
        return new Account()
                .setName("Blabla")
                .setId(99)
                .setBalance(10000)
                .setSentOperations(emptyList())
                .setAccountHolder(user)
                .setReceivedOperations(emptyList());
    }
}