package ru.demid.jbweb.web.Service;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.CategoryRepository;
import ru.demid.jbweb.web.service.CategoriesService;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CategoriesServiceTest {
    @Rule
    public ExpectedException ex = ExpectedException.none();

    @InjectMocks
    CategoriesService subj;

    @Mock
    CategoryRepository catRepo;

    @Test
    public void view_regularUse() {
        List<Category> list = Collections.singletonList(new Category("Food"));

        when(catRepo.findAll()).thenReturn(list);

        List<Category> view = subj.view();
        assertNotNull(view);
        assertEquals(view, list);

    }
    @Test
    public void view_EmptyList() {
        List<Category> list = Collections.emptyList();

        ex.expect(ServiceException.class);
        ex.expectMessage("Empty list of categories.");
        when(catRepo.findAll()).thenReturn(list);
        subj.view();

    }

    @Test
    public void setCategories_nullInput() {
        ex.expect(ServiceException.class);

        subj.setCategories(1, "");
    }

    @Test
    public void setCategories_wrongIdInput() {
        when(catRepo.getOne(9L)).thenThrow(EntityNotFoundException.class);

        ex.expect(ServiceException.class);

        subj.setCategories(9, "noname");

    }

    @Test
    public void delCategories() {
        Category category = new Category("FOOD");
        category.setId(10L);
        when(catRepo.existsById(10L)).thenReturn(true);
        when(catRepo.getOne(10L)).thenReturn(category);

        boolean b = subj.delCategories(10);
        verify(catRepo, times(1)).delete(category);
        assertTrue(b);
    }

    @Test
    public void addCategories() {
        String name = "FOOD";
        Category category = new Category(name);
        category.setId(10L);
        when((catRepo).save(Mockito.any(Category.class))).thenReturn(category);
        Long food = subj.addCategories(name);
        verify(catRepo, times(1)).save(Mockito.any(Category.class));
        assertEquals(food, category.getId());
    }

    @Test
    public void addCategories_CatchException() {
        when((catRepo).save(Mockito.any(Category.class))).thenThrow(new DataIntegrityViolationException("Cause"));
        ex.expect(ServiceException.class);
        subj.addCategories("name");
    }

    @Test
    public void addCategories_EmptyName() {
        when((catRepo).save(Mockito.any(Category.class))).thenThrow(new EntityExistsException("Cause"));
        ex.expect(ServiceException.class);
        subj.addCategories("name");
    }
}