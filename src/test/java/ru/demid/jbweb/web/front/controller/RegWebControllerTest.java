package ru.demid.jbweb.web.front.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.front.form.RegistrationForm;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;

import static java.util.Collections.emptyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(RegWebController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class RegWebControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    AuthService authService;

    @Test
    public void getRegistration() throws Exception {
        mockMvc.perform(get("/registration"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new RegistrationForm()))
                .andExpect(view().name("/registration"));
    }

    @Test
    public void postRegistration_RegularUse() throws Exception {
        when(authService.registration("bumbum@mail.ru", "password", "name", "name2")).thenReturn(
                new UserDTO(
                        2L,
                        "bumbum@mail.ru",
                        "name",
                        "name2",
                        emptyList()
                )
        );
        mockMvc.perform(post("/registration")
                .requestAttr("form", new RegistrationForm())
                    .param("email", "bumbum@mail.ru")
                    .param("password", "password")
                    .param("lastName", "name2")
                    .param("name", "name"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    public void postRegistration_NullUser() throws Exception {
        when(authService.registration("bumbum@mail.ru", "password", "name", "name2")).thenReturn(null);
        mockMvc.perform(post("/registration")
                .requestAttr("form", new RegistrationForm())
                    .param("email", "bumbum@mail.ru")
                    .param("password", "password")
                    .param("lastName", "name2")
                    .param("name", "name"))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasFieldErrors("form", "email"))
                .andExpect(view().name("/registration"));
    }

    @Test
    public void postRegistration_CatchException() throws Exception {
        when(authService.registration("bumbum@mail.ru", "password", "name", "name2"))
                .thenThrow(new ServiceException("Cause"));
        mockMvc.perform(post("/registration")
                .requestAttr("form", new RegistrationForm())
                    .param("email", "bumbum@mail.ru")
                    .param("password", "password")
                    .param("lastName", "name2")
                    .param("name", "name"))
                .andExpect(status().isOk())
                .andExpect(model().attributeHasFieldErrors("form", "email"))
                .andExpect(view().name("/registration"));
    }
}