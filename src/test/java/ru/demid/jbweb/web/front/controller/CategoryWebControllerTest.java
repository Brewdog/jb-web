package ru.demid.jbweb.web.front.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.front.form.ViewCategoriesForm;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.CategoriesService;
import ru.demid.jbweb.web.service.UserDTO;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryWebController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class CategoryWebControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    AuthService authService;
    @MockBean
    CategoriesService catService;

    @Before
    public void setUp() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(
                new UserDTO(
                        2L,
                        "petrov@gmail.com",
                        "Petr",
                        null,
                        emptyList()
                ));
    }
    private List<Category> categories = Stream.of("Cat1", "Cat2", "Cat3")
            .map(x -> new Category(x, Long.valueOf(x.substring(3))))
            .collect(Collectors.toList());

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getCategories() throws Exception {
        when(catService.view()).thenReturn(categories);
        mockMvc.perform(get("/viewCat"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", categories))
                .andExpect(view().name("catView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postCategories_DeleteRegular() throws Exception {

        when(catService.delCategories(1L)).thenReturn(true);
        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/viewCat")
        .requestAttr("form", new ViewCategoriesForm())
        .param("idCat", "-1"))
                .andExpect(status().isOk())
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("categories", categories))
                .andExpect(view().name("catView"));
        verify(catService, times(0)).setCategories(-1, null);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postCategories_RenameRegular() throws Exception {

        when(catService.setCategories(1L, "foo")).thenReturn("foo");
        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/viewCat")
        .requestAttr("form", new ViewCategoriesForm())
        .param("idCat", "1")
        .param("name", "foo"))
                .andExpect(status().isOk())
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("categories", categories))
                .andExpect(view().name("catView"));
        verify(catService, times(0)).delCategories(-1);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postCategories_RenameError() throws Exception {

        when(catService.setCategories(1L, "foo")).thenReturn(null);
        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/viewCat")
        .requestAttr("form", new ViewCategoriesForm())
        .param("idCat", "1")
        .param("name", "foo"))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors("form", "action"))
                .andExpect(view().name("catView"));
        verify(catService, times(0)).delCategories(-1);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postCategories_RenameEmptyName() throws Exception {

        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/viewCat")
        .requestAttr("form", new ViewCategoriesForm())
        .param("idCat", "1")
        .param("name", ""))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors("form", "name"))
                .andExpect(view().name("catView"));
        verify(catService, times(0)).delCategories(-1);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postCategories_DeleteHasErrors() throws Exception {

        when(catService.delCategories(1L)).thenReturn(false);
        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/viewCat")
        .requestAttr("form", new ViewCategoriesForm())
        .param("idCat", "-1"))
                .andExpect(status().isOk())
                .andExpect(model().attributeHasFieldErrors("form", "action"))
                .andExpect(model().attribute("categories", categories))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().errorCount(1))
                .andExpect(view().name("catView"));
        verify(catService, times(0)).setCategories(-1, null);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddCategory_RegularUse() throws Exception {
        when(catService.addCategories("foo")).thenReturn(1L);
        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/addCat")
                .requestAttr("form", new ViewCategoriesForm())
                .param("name", "foo"))
                .andExpect(status().isOk())
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("categories", categories))
                .andExpect(view().name("catView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddCategory_HasError() throws Exception {
        when(catService.addCategories("foo")).thenReturn(-1L);
        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/addCat")
                .requestAttr("form", new ViewCategoriesForm())
                .param("name", "foo"))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors("form", "action"))
                .andExpect(view().name("catView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddCategory_EmptyName() throws Exception {
        when(catService.view()).thenReturn(categories);

        mockMvc.perform(post("/addCat")
                .requestAttr("form", new ViewCategoriesForm())
                .param("name", ""))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors("form", "name"))
                .andExpect(view().name("catView"));
        verify(catService, times(0)).addCategories("");
    }
}