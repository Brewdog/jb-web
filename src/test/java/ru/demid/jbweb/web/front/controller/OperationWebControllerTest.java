package ru.demid.jbweb.web.front.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.front.form.AddTransactionForm;
import ru.demid.jbweb.web.front.form.ViewOperationForm;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.CategoriesService;
import ru.demid.jbweb.web.service.OperationService;
import ru.demid.jbweb.web.service.UserDTO;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(OperationWebController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class OperationWebControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    AuthService authService;

    @MockBean
    OperationService service;

    @MockBean
    CategoriesService catService;

    @Before
    public void setUp() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(
                new UserDTO(
                        2L,
                        "petrov@gmail.com",
                        "Petr",
                        null,
                        emptyList()
                ));
    }

    OperationModel getOperations(int a) {
        OperationModel opr1 = new OperationModel();
        opr1.setId(111L);
        opr1.setDate("01-01-2019");
        opr1.setSum(100L);
        OperationModel opr2 = new OperationModel();
        opr2.setId(99L);
        opr2.setDate("01-01-2019");
        opr2.setSender(new Account("Shishkoff"));
        opr2.getSender().setId(112L);
        opr2.setSum(300L);
        return a == 1 ? opr1 : opr2;
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getViewOperations() throws Exception {
        mockMvc.perform(get("/viewTrans"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new ViewOperationForm()))
                .andExpect(model().attribute("operations", new ArrayList<OperationModel>()))
                .andExpect(view().name("trnsView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postViewOperations_RegularUse() throws Exception {
        String date = "01-01-2019";
        when(service.viewByCategoryAndDate(2L, date, date)).thenReturn(Arrays.asList(getOperations(1), getOperations(2)));
        mockMvc.perform(post("/viewTrans")
                .requestAttr("form", new ViewOperationForm())
                .param("startDate", "01-01-2019")
                .param("finishDate", "01-01-2019"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("name"))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attribute("operations", Arrays.asList(getOperations(1), getOperations(2))))
                .andExpect(view().name("trnsView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postViewOperations_WrongDateFormat() throws Exception {
        String date = "01/01.2019";
        when(service.viewByCategoryAndDate(2L, date, date)).thenReturn(Arrays.asList(getOperations(1), getOperations(2)));
        mockMvc.perform(post("/viewTrans")
                .requestAttr("form", new ViewOperationForm())
                .param("startDate", date)
                .param("finishDate", date))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeExists("name"))
                .andExpect(model().attributeExists("operations"))
                .andExpect(model().attributeHasFieldErrors("form", "startDate"))
                .andExpect(model().attributeHasFieldErrors("form", "finishDate"))
                .andExpect(view().name("trnsView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postViewOperations_Null() throws Exception {
        String date = "01-01-2019";
        when(service.viewByCategoryAndDate(2L, date, date)).thenReturn(null);
        mockMvc.perform(post("/viewTrans")
                .requestAttr("form", new ViewOperationForm())
                .param("startDate", date)
                .param("finishDate", date))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeExists("name"))
                .andExpect(model().attributeExists("operations"))
                .andExpect(model().attributeExists("form"))
                .andExpect(model().attributeHasFieldErrors("form", "startDate"))
                .andExpect(view().name("trnsView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postViewOperations_EmptyDate() throws Exception {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
        ViewOperationForm form = new ViewOperationForm();
        form.setStartDate(date);
        form.setFinishDate(date);
        when(service.viewByCategoryAndDate(2L, date, date))
                .thenReturn(Arrays.asList(getOperations(1), getOperations(2)));
        mockMvc.perform(post("/viewTrans")
                .param("startDate", "")
                .param("finishDate", ""))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("name"))
                .andExpect(model().attribute("operations", Arrays.asList(getOperations(1), getOperations(2))))
                .andExpect(model().attribute("form", form))
                .andExpect(view().name("trnsView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getAddTransaction() throws Exception {
        mockMvc.perform(get("/addTrans"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new AddTransactionForm()))
                .andExpect(model().attribute("accounts", emptyList()))
                .andExpect(view().name("trnsAdd"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddTransaction_RegularUse() throws Exception {
        String date = "01-01-2019";
        when(service.addNewOperation(2L,
                112L,
                null,
                300,
                date,
                null,
                null))
                .thenReturn(getOperations(2));

        mockMvc.perform(post("/addTrans")
                .requestAttr("form", new AddTransactionForm())
                .param("sum", "300")
                .param("date", date)
                .param("senderAcc.id", "112"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new ViewOperationForm()))
                .andExpect(model().attribute("operations", singletonList(getOperations(2))))
                .andExpect(view().name("trnsView"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddTransaction_ExceptionCatch() throws Exception {
        String date = "1-1-2019";
        Account account = new Account();
        account.setId(112L);
        account.setName("Shishkoff");
        account.setSentOperations(singletonList(getOperations(2)));
        List<Category> categories = Stream.of("Cat1", "Cat2", "Cat3")
                .map(x -> new Category(x, Long.valueOf(x.substring(3))))
                .collect(Collectors.toList());
        when(service.addNewOperation(2L,
                112L,
                null,
                300,
                date,
                null,
                null))
                .thenThrow(new ServiceException("Cause"));

        mockMvc.perform(post("/addTrans")
                .requestAttr("form", new AddTransactionForm())
                .param("sum", String.valueOf(300))
                .param("date", date)
                .param("senderAcc", account.toString())
                .requestAttr("accounts", singletonList(account))
                .requestAttr("categories", categories))

                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(model().attributeHasFieldErrors("form", "sender"))
                .andExpect(view().name("trnsAdd"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddTransaction_HasErrorsAndEmptyDate() throws Exception {
        String date = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
        Account account = new Account();
        account.setId(112L);
        account.setName("Shishkoff");
        account.setSentOperations(singletonList(getOperations(2)));
        List<Category> categories = Stream.of("Cat1", "Cat2", "Cat3")
                .map(x -> new Category(x, Long.valueOf(x.substring(3))))
                .collect(Collectors.toList());
        when(service.addNewOperation(2L,
                112L,
                null,
                300,
                date,
                null,
                null))
                .thenThrow(new ServiceException("Cause"));

        mockMvc.perform(post("/addTrans")
                .requestAttr("form", new AddTransactionForm())
                .param("sum", "")
                .param("date", "")
                .param("senderAcc", account.toString())
                .requestAttr("accounts", singletonList(account))
                .requestAttr("categories", categories))

                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors("form", "sum"))
                .andExpect(view().name("trnsAdd"));
    }
}