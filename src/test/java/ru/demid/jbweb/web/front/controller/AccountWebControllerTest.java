package ru.demid.jbweb.web.front.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.front.form.AddAccForm;
import ru.demid.jbweb.web.front.form.UpdAccForm;
import ru.demid.jbweb.web.service.AccountService;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebMvcTest(AccountWebController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class AccountWebControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    AuthService authService;
    @MockBean
    AccountService service;

    @Before
    public void setUp() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(
                new UserDTO(
                        2,
                        "petrov@gmail.com",
                        "Petr",
                        null,
                        emptyList()
                ));
    }

    private List<Account> getTestAccounts() {
        return singletonList(new Account()
                .setName("Blabla")
                .setId(99)
                .setBalance(1000f)
                .setSentOperations(emptyList())
                .setReceivedOperations(emptyList()));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getAccounts() throws Exception {
        mockMvc.perform(get("/accounts"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("name", "Petr"))
                .andExpect(model().attribute("accounts", emptyList()))
                .andExpect(view().name("accounts"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getUpdAccounts() throws Exception {
        mockMvc.perform(get("/updAcc"))
                .andExpect(status().isOk())
                .andExpect(model().hasNoErrors())
                .andExpect(model().attribute("form", new UpdAccForm()))
                .andExpect(model().attribute("name", "Petr"))
                .andExpect(model().attribute("accounts", emptyList()))
                .andExpect(view().name("accUpd"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postUpdAccounts() throws Exception {
        when(service.updateInfo(2, 33, "Blabla", 10000L)).thenReturn(getTestAccounts().get(0));

        mockMvc.perform(post("/updAcc").requestAttr("form", new UpdAccForm())
                        .param("idAcc", "33")
                        .param("name", "Blabla")
                        .param("balance", "10000"))
                .andExpect(model().hasNoErrors())
                .andExpect(status().is3xxRedirection())
                .andExpect(model().attribute("newAccountName", "Blabla"))
                .andExpect(view().name("redirect:/accounts"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postUpdAccounts_NullAccount() throws Exception {
        when(service.updateInfo(2, 33, "Blabla", 10000L)).thenReturn(null);

        mockMvc.perform(post("/updAcc").requestAttr("form", new UpdAccForm())
                        .param("idAcc", "33")
                        .param("name", "Blabla")
                        .param("balance", "10000"))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors("form", "name"))
                .andExpect(model().attribute("name", "Petr"))
                .andExpect(view().name("accUpd"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getAddAccount() throws Exception {
        mockMvc.perform(get("/addAcc"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new AddAccForm()))
                .andExpect(view().name("accAdd"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddAccount_RegularUse() throws Exception {
        when(service.makeAccount( "Blabla", 2)).thenReturn(getTestAccounts().get(0));

        mockMvc.perform(post("/addAcc").requestAttr("form", new AddAccForm())
                .param("name", "Blabla"))
                .andExpect(model().hasNoErrors())
                .andExpect(status().is3xxRedirection())
                .andExpect(model().attribute("name", "Petr"))
                .andExpect(view().name("redirect:/accounts"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddAccount_NullAccount() throws Exception {
        when(service.makeAccount( "Blabla", 2)).thenReturn(null);

        mockMvc.perform(post("/addAcc").requestAttr("form", new AddAccForm())
                .param("name", "Blabla"))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attribute("name", "Petr"))
                .andExpect(model().attributeExists("form"))
                .andExpect(view().name("accAdd"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void postAddAccount_SameName() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(
                new UserDTO(
                        2,
                        "petrov@gmail.com",
                        "Petr",
                        null,
                        getTestAccounts()
                ));

        mockMvc.perform(post("/addAcc").requestAttr("form", new AddAccForm())
                .param("name", "Blabla"))
                .andExpect(status().isOk())
                .andExpect(model().errorCount(1))
                .andExpect(model().attributeHasFieldErrors("form", "name"))
                .andExpect(model().attribute("name", "Petr"))
                .andExpect(model().attributeExists("form"))
                .andExpect(view().name("accAdd"));
        verify(service, times(0)).makeAccount("Blabla", 2);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getDeleteAccount() throws Exception {
        when(service.viewAccounts(2L)).thenReturn(getTestAccounts());
        mockMvc.perform(get("/delAcc"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("name", "Petr"))
                .andExpect(model().attribute("accounts", getTestAccounts()))
                .andExpect(view().name("accDel"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void getDeleteAccount_NoAccounts() throws Exception {
        when(service.viewAccounts(2L)).thenReturn(emptyList());
        mockMvc.perform(get("/delAcc"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/accounts"));
    }

    @Test
    public void postDeleteAccount() {
    }
}