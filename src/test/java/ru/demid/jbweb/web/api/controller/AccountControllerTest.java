package ru.demid.jbweb.web.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.service.AccountService;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;

import java.util.Collections;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class AccountControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AccountService service;

    @MockBean
    AuthService authService;

    private UserDTO getUser() {
        return new UserDTO(
                2L,
                "petrov@gmail.com",
                null,
                null,
                null
        );
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accView_RegularUse() throws Exception {
        Account account = new Account();
        account.setName("Сбербанк");
        account.setBalance(100000f);
        account.setId(4L);
        account.setReceivedOperations(emptyList());
        account.setSentOperations(emptyList());
        when(authService.getUserByID(2L)).thenReturn(
                getUser());
        when(service.viewAccounts(2L)).thenReturn(Collections.singletonList(account));

        mockMvc.perform(get("/api/accView"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"accounts\": [\n" +
                        "        {\n" +
                        "            \"id\": 4,\n" +
                        "            \"name\": \"Сбербанк\",\n" +
                        "            \"balance\": 100000.0,\n" +
                        "            \"sentOperations\": [],\n" +
                        "            \"receivedOperations\": []\n" +
                        "        }\n" +
                        "    ]\n" +
                        "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accView_NoAccount() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(
                getUser());
        when(service.viewAccounts(2L)).thenReturn(emptyList());

        mockMvc.perform(get("/api/accView"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"accounts\": []\n" +
                        "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accAdd_RegularUse() throws Exception {
        Account account = new Account();
        account.setName("Sberbank");
        account.setBalance(100000f);
        account.setId(4L);
        when(authService.getUserByID(2L)).thenReturn(
                getUser());
        when(service.makeAccount("Sberbank", 2L)).thenReturn(account);

        mockMvc.perform(post("/api/addAcc")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"name\":\"Sberbank\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"name\": \"Sberbank\",\n" +
                        "    \"balance\": 100000.0,\n" +
                        "    \"accountId\": 4\n" +
                        "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accAdd_NameExist() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(
                getUser());
        when(service.makeAccount("Sberbank", 2L)).thenThrow(new ServiceException("Name already exist"));

        mockMvc.perform(post("/api/addAcc")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"name\":\"Sberbank\"\n" +
                        "}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accDel_RegularUse() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(getUser());
        when(service.delAccount(4L, 2L)).thenReturn(true);
        when(service.viewAccounts(2L)).thenReturn(emptyList());
        mockMvc.perform(post("/api/accDel")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"idAcc\":4\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accDel_FailDelete() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(getUser());
        when(service.delAccount(4L, 2L)).thenReturn(false);
        mockMvc.perform(post("/api/accDel")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"idAcc\":4\n" +
                        "}"))
                .andExpect(status().is5xxServerError());
        verify(service, times(0)).viewAccounts(2L);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accUpd_RegularUse() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(getUser());

        Account account = new Account();
        account.setName("Tinkoff");
        account.setBalance(100000f);
        account.setId(4L);
        account.setReceivedOperations(emptyList());
        account.setSentOperations(emptyList());

        when(service.updateInfo(getUser().getId(), 4L, "Tinkoff", 100000L)).thenReturn(account);
        when(service.viewAccounts(2L)).thenReturn(singletonList(account));

        mockMvc.perform(post("/api/accUpd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"name\": \"Tinkoff\",\n" +
                        "    \"balance\": 100000.0,\n" +
                        "    \"idAcc\": 4\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json(
                        "{\n" +
                                "    \"accounts\": [\n" +
                                "        {\n" +
                                "            \"id\": 4,\n" +
                                "            \"name\": \"Tinkoff\",\n" +
                                "            \"balance\": 100000.0,\n" +
                                "            \"sentOperations\": [],\n" +
                                "            \"receivedOperations\": []\n" +
                                "        }\n" +
                                "    ]\n" +
                                "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accUpd_FailedUpdate() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(getUser());

        when(service.updateInfo(getUser().getId(), 4L, "Tinkoff", 100000L)).thenReturn(null);

        mockMvc.perform(post("/api/accUpd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"name\": \"Tinkoff\",\n" +
                        "    \"balance\": 100000.0,\n" +
                        "    \"idAcc\": 4\n" +
                        "}"))
                .andExpect(status().is4xxClientError());

        verify(service, times(0)).viewAccounts(2L);
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void accUpd_ExceptionCatch() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(getUser());

        when(service.updateInfo(getUser().getId(), 4L, "Tinkoff", 100000L)).thenThrow(new ServiceException("Account not found"));

        mockMvc.perform(post("/api/accUpd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"name\": \"Tinkoff\",\n" +
                        "    \"balance\": 100000.0,\n" +
                        "    \"idAcc\": 4\n" +
                        "}"))
                .andExpect(status().is4xxClientError());

        verify(service, times(0)).viewAccounts(2L);
    }
}