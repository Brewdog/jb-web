package ru.demid.jbweb.web.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class RegistrationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuthService service;

    @Test
    public void registration_RegularUse() throws Exception {
        UserDTO userDTO = new UserDTO(
                2L,
                "petrov@gmail.com",
                "Petr",
                "Petroff",
                null
        );
        when(service.registration("petrov@gmail.com", "123123", "Petr", "Petroff")).thenReturn(userDTO);

        mockMvc.perform(post("/api/reg")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Petr\",\n" +
                        "  \"lastName\": \"Petroff\",\n" +
                        "  \"email\": \"petrov@gmail.com\",\n" +
                        "  \"password\": \"123123\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"name\": \"Petr\",\n" +
                        "  \"email\": \"petrov@gmail.com\",\n" +
                        "  \"userId\": 2\n" +
                        "}"));
    }

    @Test
    public void registration_CatchException() throws Exception {
        when(service.registration("petrov@gmail.com", "123123", "Petr", "Petroff")).thenThrow(new ServiceException("Cause"));

        mockMvc.perform(post("/api/reg")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Petr\",\n" +
                        "  \"lastName\": \"Petroff\",\n" +
                        "  \"email\": \"petrov@gmail.com\",\n" +
                        "  \"password\": \"123123\"\n" +
                        "}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
                .andExpect(content().string("Cause"));
    }
}