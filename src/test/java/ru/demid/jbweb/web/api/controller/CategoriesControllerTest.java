package ru.demid.jbweb.web.api.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.api.converter.ListOfCategoryToMapOfLongAndStringConverter;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.CategoriesService;
import ru.demid.jbweb.web.service.UserDTO;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoriesController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class CategoriesControllerTest {
    @MockBean
    CategoriesService service;
    @MockBean
    AuthService authService;
    @SpyBean
    ListOfCategoryToMapOfLongAndStringConverter converter;
    @Autowired
    MockMvc mockMvc;

    private List<Category> categories = Stream.of("Cat1", "Cat2", "Cat3")
            .map(x -> new Category(x, Long.valueOf(x.substring(3))))
            .collect(Collectors.toList());


    @Before
    public void setUp() throws Exception {
        when(authService.getUserByID(2L)).thenReturn(
                new UserDTO(
                        2L,
                        "petrov@gmail.com",
                        null,
                        null,
                        null
                ));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catView_RegularUse() throws Exception {
        when(service.view()).thenReturn(categories);
        mockMvc.perform(get("/api/catView"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"categories\": {\n" +
                        "    \"1\": \"Cat1\",\n" +
                        "    \"2\": \"Cat2\",\n" +
                        "    \"3\": \"Cat3\"\n" +
                        "  }\n" +
                        "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catView_NoCategories() throws Exception {
        when(service.view()).thenThrow(new ServiceException("Empty list of categories."));
        mockMvc.perform(get("/api/catView"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catAdd_RegularUse() throws Exception {
        when(service.addCategories("Cat4")).thenReturn(4L);
        categories.add(new Category("Cat4", 4L));
        when(service.view()).thenReturn(categories);

        mockMvc.perform(post("/api/catAdd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Cat4\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"result\":\n" +
                        "          " +
                        "[" +
                        " " +
                        "{\"id\":1,\"name\":\"Cat1\"},\n" +
                        "            " +
                        "{\"id\":2,\"name\":\"Cat2\"},\n" +
                        "            " +
                        "{\"id\":3,\"name\":\"Cat3\"},\n" +
                        "            " +
                        "{\"id\":4,\"name\":\"Cat4\"}\n" +
                        "          " +
                        "],\n" +
                        "  " +
                        "\"message\":\"Category has been added successfully\"}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catAdd_Failed() throws Exception {
        when(service.addCategories("Cat4")).thenReturn(-1L);
        mockMvc.perform(post("/api/catAdd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Cat4\"\n" +
                        "}"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catAdd_CatchException() throws Exception {
        when(service.addCategories("Cat4")).thenThrow(new ServiceException("Cause"));
        mockMvc.perform(post("/api/catAdd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Cat4\"\n" +
                        "}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().json("{\n" +
                        "  " +
                        "\"result\":[],\n" +
                        "  \"message\":\"Cause\"\n" +
                        "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catUpd_RegularUse() throws Exception {
        when(service.setCategories(1L, "Cat9")).thenReturn("Cat9");
        categories.set(0, new Category("Cat9", 1L));
        when(service.view()).thenReturn(categories);

        mockMvc.perform(post("/api/catUpd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Cat9\",\n" +
                        "  \"idCat\": 1\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  " +
                        "\"result\":\n" +
                        "          {\n" +
                        "            \"1\":\"Cat9\",\n" +
                        "            \"2\":\"Cat2\",\n" +
                        "            \"3\":\"Cat3\"\n" +
                        "          },\n" +
                        "  " +
                        "\"message\":\"Information has been updated\"\n" +
                        "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catUpd_CatchException() throws Exception {
        when(service.setCategories(1L, "Cat9")).thenThrow(new ServiceException("Cause"));
        categories.set(0, new Category("Cat9", 1L));
        when(service.view()).thenReturn(categories);

        mockMvc.perform(post("/api/catUpd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Cat9\",\n" +
                        "  \"idCat\": 1\n" +
                        "}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().json("{\n" +
                        "  " +
                        "\"result\":{},\n" +
                        "  \"message\":\"Cause\"\n" +
                        "}"));
        verify(service, times(0)).view();
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catUpd_FailedUpdate() throws Exception {
        when(service.setCategories(1L, "Cat9")).thenReturn(null);

        mockMvc.perform(post("/api/catUpd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"name\": \"Cat9\",\n" +
                        "  \"idCat\": 1\n" +
                        "}"))
                .andExpect(status().isNoContent());
        verify(service, times(0)).view();
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catDel_RegularUse() throws Exception {
        when(service.delCategories(1L)).thenReturn(true);
        when(service.view()).thenReturn(categories);

        mockMvc.perform(post("/api/catDel")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"idCat\": 1\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  " +
                        "\"result\":\n" +
                        "    [\n" +
                        "      {\"id\":1,\"name\":\"Cat1\"},\n" +
                        "      {\"id\":2,\"name\":\"Cat2\"},\n" +
                        "      {\"id\":3,\"name\":\"Cat3\"}\n" +
                        "    ],\n" +
                        "  \"message\":\"Category has been deleted successfully\"}"));

    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catDel_CatchException() throws Exception {
        when(service.delCategories(1L)).thenThrow(new ServiceException("Cause"));
        mockMvc.perform(post("/api/catDel")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"idCat\": 1\n" +
                        "}"))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(content().json("{\n" +
                        "  " +
                        "\"result\":[],\n" +
                        "  \"message\":\"Cause\"\n" +
                        "}"));

        verify(service, times(0)).view();
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void catDel_FailedDelete() throws Exception {
        when(service.delCategories(1L)).thenReturn(false);
        mockMvc.perform(post("/api/catDel")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"idCat\": 1\n" +
                        "}"))
                .andExpect(status().isNoContent());

        verify(service, times(0)).view();
    }
}