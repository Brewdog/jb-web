package ru.demid.jbweb.web.api.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.demid.jbweb.web.MockSecurityConfiguration;
import ru.demid.jbweb.web.WebApplication;
import ru.demid.jbweb.web.config.SecurityConfig;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.OperationService;
import ru.demid.jbweb.web.service.UserDTO;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
@ContextConfiguration(classes = WebApplication.class)
@Import({SecurityConfig.class, MockSecurityConfiguration.class})
public class TransactionControllerTest {

    @MockBean
    AuthService authService;

    @MockBean
    OperationService service;

    @Autowired
    MockMvc mockMvc;


    @Before
    public void setUp() {
        when(authService.getUserByID(2L)).thenReturn(
                new UserDTO(
                        2L,
                        "petrov@gmail.com",
                        null,
                        null,
                        null
                ));
    }

    OperationModel getOperations(int a) {
        OperationModel opr1 = new OperationModel();
        opr1.setId(111L);
        opr1.setDate("01-01-2019");
        opr1.setSum(100L);
        OperationModel opr2 = new OperationModel();
        opr2.setId(99L);
        opr2.setDate("01-01-2019");
        opr2.setSum(300L);
        return a == 1 ? opr1 : opr2;
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void trnsAdd_RegularUse() throws Exception {
        String date = "01-01-2019";

        when(service.addNewOperation(2L, 112L, null, 300, date, null, emptyList())).thenReturn(getOperations(2));

        mockMvc.perform(post("/api/transAdd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"idSenderAcc\": 112,\n" +
                        "  \"idAccepterAcc\": null,\n" +
                        "  \"summ\": 300,\n" +
                        "  \"date\": \"01-01-2019\",\n" +
                        "  \"comment\": null,\n" +
                        "  \"categories\": []\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "    \"result\": {\n" +
                        "        \"id\": 99,\n" +
                        "        \"sum\": 300,\n" +
                        "        \"date\": \"01-01-2019\",\n" +
                        "        \"comment\": null,\n" +
                        "        \"categories\": null\n" +
                        "    }\n" +
                        "}"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void trnsAdd_CatchException() throws Exception {
        String date = "01-01-2019";

        when(service.addNewOperation(2L, 112L, null, 300, date, null, emptyList())).thenThrow(new ServiceException("Cause"));

        mockMvc.perform(post("/api/transAdd")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"idSenderAcc\": 112,\n" +
                        "  \"idAccepterAcc\": null,\n" +
                        "  \"summ\": 300,\n" +
                        "  \"date\": \"01-01-2019\",\n" +
                        "  \"comment\": null,\n" +
                        "  \"categories\": []\n" +
                        "}"))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void trnsView_RegularUse() throws Exception {
        String date = "01-01-2019";
        when(service.viewByCategoryAndDate(2L, date, date)).thenReturn(Arrays.asList(getOperations(1), getOperations(2)));
        mockMvc.perform(post("/api/transView")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"id\": 2,\n" +
                        "  \"startDate\": \"01-01-2019\",\n" +
                        "  \"finishDate\": \"01-01-2019\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  " +
                        "\"transactions\":\n" +
                        "      [\n" +
                        "        {\n" +
                        "          \"id\":111,\n" +
                        "          \"sum\":100,\n" +
                        "          \"date\":\"01-01-2019\",\n" +
                        "          \"comment\":null,\n" +
                        "          \"categories\":null\n" +
                        "        },\n" +
                        "        {\n" +
                        "          \"id\":99,\n" +
                        "          \"sum\":300,\n" +
                        "          \"date\":\"01-01-2019\",\n" +
                        "          \"comment\":null,\n" +
                        "          \"categories\":null\n" +
                        "        }\n" +
                        "      ]\n" +
                        "}\n"));
    }

    @Test
    @WithUserDetails(value = "petrov@gmail.com", userDetailsServiceBeanName = "userDetailsService")
    public void trnsView_CatchException() throws Exception {
        String date = "01-01-2019";
        when(service.viewByCategoryAndDate(2L, date, date)).thenThrow(new ServiceException("Cause"));
        mockMvc.perform(post("/api/transView")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"id\": 2,\n" +
                        "  \"startDate\": \"01-01-2019\",\n" +
                        "  \"finishDate\": \"01-01-2019\"\n" +
                        "}"))
                .andExpect(status().isNotFound());
    }
}