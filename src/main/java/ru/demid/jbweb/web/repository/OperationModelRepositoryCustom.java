package ru.demid.jbweb.web.repository;

import ru.demid.jbweb.web.entity.OperationModel;

import java.util.List;

public interface OperationModelRepositoryCustom {

    List<OperationModel> findByFilter(FilterForOperation filter);
}
