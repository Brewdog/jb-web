package ru.demid.jbweb.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.demid.jbweb.web.entity.Account;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    @Query("select a from Account a left join fetch a.accountHolder where a.accountHolder.id = :id")
    List<Account> findAccounts(Long id);

    boolean findAccountByName(String name);
}
