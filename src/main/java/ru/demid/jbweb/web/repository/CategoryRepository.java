package ru.demid.jbweb.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.demid.jbweb.web.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
