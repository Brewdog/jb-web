package ru.demid.jbweb.web.repository;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.demid.jbweb.web.entity.Category;

import javax.validation.constraints.NotEmpty;
import java.util.Collections;
import java.util.List;

@Data
@Accessors(chain = true)
public class FilterForOperation {
    @NotEmpty
    Long idHolder;
    String startDate;
    String finishDate;
    List<Category> listCategory = Collections.emptyList();
}
