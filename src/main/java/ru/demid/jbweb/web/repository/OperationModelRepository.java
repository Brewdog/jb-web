package ru.demid.jbweb.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.demid.jbweb.web.entity.OperationModel;

import java.util.List;

@Repository
public interface OperationModelRepository extends JpaRepository<OperationModel, Long>, OperationModelRepositoryCustom {
    @Query("select o from OperationModel o  left join o.acceptor left join o.sender where function('to_date', o.date, 'dd-MM-YYYY') between function('to_date', :start, 'dd-MM-YYYY') and function('to_date', :finish, 'dd-MM-YYYY') and (o.sender.accountHolder.id = :id or o.acceptor.accountHolder.id = :id)")
    List<OperationModel> findByHolderIdAndDate(@Param("id") Long id, @Param("start") String start, @Param("finish") String finish);

    List<OperationModel> findByFilter (FilterForOperation filter);
}
