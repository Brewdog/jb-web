package ru.demid.jbweb.web.repository;

import lombok.RequiredArgsConstructor;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class OperationModelRepositoryImpl implements OperationModelRepositoryCustom {
    private final EntityManager em;
    @Override
    public List<OperationModel> findByFilter(FilterForOperation filter) {
        //language=JPAQL
        String query = "select o from OperationModel o left join o.acceptor left join o.sender where 1=1";
        Map<String, Object> params = new HashMap<>();

        if (filter.getStartDate()==null && filter.getFinishDate()==null){
            throw new ServiceException("Date required");
        }

        if (filter.getStartDate()!=null){
            query += " and function('to_date', o.date, 'dd-MM-YYYY') >= function('to_date', :start, 'dd-MM-YYYY')";
            params.put("start", filter.getStartDate());
        }
        if (filter.getFinishDate()!=null){
            query += " and function('to_date', o.date, 'dd-MM-YYYY') <= function('to_date', :finish, 'dd-MM-YYYY')";
            params.put("finish", filter.getFinishDate());
        }
        if (!filter.getListCategory().isEmpty()){
            query+=" and :category member of o.categories";
            params.put("category", filter.getListCategory());
        }
        if (filter.getIdHolder() == null){
            throw new ServiceException("Problem with holder ID");
        }
        query += " and (o.sender.accountHolder.id = :id or o.acceptor.accountHolder.id = :id)";
        params.put("id", filter.getIdHolder());

        TypedQuery<OperationModel> typedQuery = em.createQuery(query, OperationModel.class);
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            typedQuery.setParameter(entry.getKey(), entry.getValue());
        }

        return typedQuery.getResultList();
    }
}
