package ru.demid.jbweb.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import ru.demid.jbweb.web.console.ConsoleRunner;
@Profile("Console")
@ComponentScan(basePackages = "ru.demid.jbweb.web",
        excludeFilters = @ComponentScan.Filter(SpringBootApplication.class))
public class ConsoleApp implements CommandLineRunner {
    @Autowired
    ConsoleRunner runner;

    public static void main(String[] args) {
        SpringApplication.run(ConsoleApp.class, args);
    }

    @Override
    public void run(String... args) {
        runner.runConsole();
    }
}
