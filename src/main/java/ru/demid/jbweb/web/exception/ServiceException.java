package ru.demid.jbweb.web.exception;

public class ServiceException extends RuntimeException {
    public ServiceException(Throwable cause, String message) {
        super(message, cause);
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
