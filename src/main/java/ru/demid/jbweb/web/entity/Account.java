package ru.demid.jbweb.web.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Table(name = "account")
@RequiredArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    @NotNull
    @NonNull
    private String name;

    @Column(name = "balance")
    private float balance;

    @ToString.Exclude
    @OneToMany(mappedBy = "sender", fetch = FetchType.LAZY)
    private List<OperationModel> sentOperations;

    @ToString.Exclude
    @OneToMany(mappedBy = "acceptor", fetch = FetchType.LAZY)
    private List<OperationModel> receivedOperations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "holder_id")
    @NotNull
    @ToString.Exclude
    @JsonBackReference
    private UserModel accountHolder;
}
