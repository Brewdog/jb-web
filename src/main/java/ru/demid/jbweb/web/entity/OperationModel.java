package ru.demid.jbweb.web.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Table(name = "operation")
@RequiredArgsConstructor
public class OperationModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    @NotNull
    private long sum;

    @JoinColumn(name = "sender")
    @ManyToOne
    @JsonBackReference
    @ToString.Exclude
    private Account sender;

    @JoinColumn(name = "acceptor")
    @ManyToOne
    @JsonBackReference
    @ToString.Exclude
    private Account acceptor;

    @Column
    @NotEmpty
    private String date;

    @Column
    private String comment;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "category_to_operation",
            joinColumns = @JoinColumn(name = "operation_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id")
    )
    private List<Category> categories;
}
