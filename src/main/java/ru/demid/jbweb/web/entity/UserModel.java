package ru.demid.jbweb.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ru.demid.jbweb.web.security.UserRoles;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

import static java.util.Collections.emptySet;

@Data
@Entity
@Table(name = "holder")
@RequiredArgsConstructor
@NoArgsConstructor
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column()
    @Email
    @NonNull
    private String email;

    @Column
    @NotBlank
    @NonNull
    @JsonIgnore
    private String password;

    @Column(name = "first_name")
    @NonNull
    @NotBlank
    private String name;

    @Column(name = "second_name")
    @NonNull
    @NotBlank
    private String lastName;

    @OneToMany(mappedBy = "accountHolder", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Account> accounts;

    @ElementCollection(targetClass = UserRoles.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "role")
    private Set<UserRoles> userRoles = emptySet();
}
