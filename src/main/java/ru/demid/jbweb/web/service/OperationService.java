package ru.demid.jbweb.web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.AccountRepository;
import ru.demid.jbweb.web.repository.CategoryRepository;
import ru.demid.jbweb.web.repository.OperationModelRepository;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OperationService {

    private final OperationModelRepository operationRepo;
    private final AccountRepository accountRepo;
    private final CategoryRepository categoryRepo;

    public List<OperationModel> viewByCategoryAndDate(long id, String startDate, String finishDate) throws ServiceException {
        List<OperationModel> list;
        try {
            list = operationRepo.findByHolderIdAndDate(id, startDate, finishDate);
            if (list.isEmpty()) {
                throw new ServiceException("No transactions found");
            }
        } catch (DataAccessException e) {
            throw new ServiceException(e.getRootCause());
        }
        return list;
    }

    @Transactional
    public OperationModel addNewOperation(long idHolder, Long idSenderAccount, Long idAccepterAccount, long sum, String date, String comment, List<Category> categories) throws ServiceException {
        if (idAccepterAccount == null && idSenderAccount == null) {
            throw new ServiceException("No accounts indicated");
        } else {
            if ((idAccepterAccount != null && idSenderAccount != null) && idAccepterAccount.equals(idSenderAccount)) {
                throw new ServiceException("Same account");
            }
        }
        try {
            OperationModel resultOperation = new OperationModel();
            if (idAccepterAccount != null) {
                Account accepterAccount = accountRepo.getOne(idAccepterAccount);
                if (accepterAccount.getAccountHolder().getId() != idHolder) {
                    throw new ServiceException("Wrong holder's ID");
                }
                accepterAccount.setBalance(accepterAccount.getBalance() + sum);
                resultOperation.setAcceptor(accepterAccount);
            }
            if (idSenderAccount != null) {
                Account senderAccount = accountRepo.getOne(idSenderAccount);
                if (senderAccount.getAccountHolder().getId() != idHolder) {
                    throw new ServiceException("Wrong holder's ID");
                }
                senderAccount.setBalance(senderAccount.getBalance() - sum);
                resultOperation.setSender(senderAccount);
            }

            if (!categories.isEmpty()) {
                resultOperation.setCategories(categories);
            } else {
                List<Category> defaultCategories = Collections.singletonList(categoryRepo.getOne(6L));
                resultOperation.setCategories(defaultCategories);
            }
            resultOperation.setDate(date);
            resultOperation.setSum(sum);
            if (comment != null) {
                resultOperation.setComment(comment);
            }
            operationRepo.save(resultOperation);

            return resultOperation;

        } catch (DataAccessException e) {
            throw new ServiceException(e.getRootCause());
        }
    }

//    public OperationDTO addNewOperation(long idHolder, Long idSenderAccount, Long idAccepterAccount, long sum, String date, String comment, List<Category> categories) {
//        if (categories == null || categories.isEmpty()) {
//            return addNewOperation(idHolder, idSenderAccount, idAccepterAccount, sum, date, comment);
//        } else {
//            if (idAccepterAccount == null && idSenderAccount == null) {
//                throw new ServiceException("No accounts indicated");
//            } else {
//                if ((idAccepterAccount != null && idSenderAccount != null) && idAccepterAccount.equals(idSenderAccount)) {
//                    throw new ServiceException("Same account");
//                }
//            }
//            try {
//                return converterToDTO.convert(operationRepo.addOperation(idHolder, idSenderAccount, idAccepterAccount, sum, date, comment, categories));
//            } catch (CustomException e) {
//                throw new ServiceException(e.getMessage());
//            }
//        }
//    }
}
