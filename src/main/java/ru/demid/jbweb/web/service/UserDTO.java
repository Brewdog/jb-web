package ru.demid.jbweb.web.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.demid.jbweb.web.entity.Account;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class UserDTO {
    private long id;
    private String email;
    private String name;
    private String lastName;
    private List<Account> accounts;
}
