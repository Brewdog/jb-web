package ru.demid.jbweb.web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.CategoryRepository;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoriesService {
    private final CategoryRepository catRepo;

    public List<Category> view() {
        List<Category> categories = catRepo.findAll();
        if (categories.isEmpty()) {
            throw new ServiceException("Empty list of categories.");
        }
        return categories.stream().sorted(Comparator.comparing(Category::getId)).collect(Collectors.toList());
    }

    public String setCategories(long id, String name) {
        String result;
        if (name.isEmpty()){
            throw new ServiceException("Name cant be empty");
        }
        try {
            catRepo.getOne(id).setName(name);
            catRepo.flush();
            result = name;

        } catch (EntityNotFoundException | DataAccessException e) {
            throw new ServiceException(e.getCause(), e.getMessage());
        }
        return result;
    }

    public boolean delCategories(long id) {
        try {
            if (catRepo.existsById(id)){
            Category one = catRepo.getOne(id);
            catRepo.delete(one);
            catRepo.flush();
            return true;}
            return false;

        } catch (DataAccessException e) {
            throw new ServiceException(e.getCause());
        }

    }

    public long addCategories(String name) {
        if (name.isEmpty()){
            throw new ServiceException("Name cant be empty");
        }
        long result;
        try {
            Category newCategory = catRepo.save(new Category(name));
            result = newCategory.getId();

        } catch (PersistenceException | DataAccessException e) {
            throw new ServiceException(e, e.getMessage());
        }
        return result;
    }
}
