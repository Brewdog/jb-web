package ru.demid.jbweb.web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.AccountRepository;
import ru.demid.jbweb.web.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public List<Account> viewAccounts(long id) {
        if (!accountRepository.existsById(id)) {
            throw new ServiceException("Account with holder id: " + id + " not found");
        }
        return accountRepository.findAccounts(id);
    }

    @Transactional
    public Account makeAccount(String name, long idHolder) {
        if (viewAccounts(idHolder).stream().anyMatch(x -> x.getName().toLowerCase().equals(name.toLowerCase()))) {
            throw new ServiceException("Name already exist");
        }
        Account account = new Account();
        account.setName(name);
        account.setAccountHolder(userRepository.getOne(idHolder));
        return accountRepository.save(account);
    }

    public boolean delAccount(long idAccount, long idHolder) {
        try {
            Account account = accountRepository.getOne(idAccount);
            if (account.getAccountHolder().getId() != idHolder) {
                return false;
            }
            accountRepository.delete(account);
            return true;

        } catch (EntityNotFoundException | DataAccessException e) {
            throw new ServiceException(e, "Account not found");
        }
    }

    //todo перегрузить метод на изменение только баланса
    @Transactional
    public Account updateInfo(long idHolder, long idAccount, String name, Long balance) {
        try {
            Account account = accountRepository.getOne(idAccount);
            if (account.getAccountHolder().getId() != idHolder) {
                throw new ServiceException("Wrong holder id");
            }
            if (balance != null) {
                account.setBalance(balance);
            }
            account.setName(name);

            return accountRepository.saveAndFlush(account);

        } catch (EntityNotFoundException e) {
            throw new ServiceException(e, "Account not found");
        }
    }
}


