package ru.demid.jbweb.web.service;

import org.springframework.stereotype.Service;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

@Service
public class Md5DigestService implements DigestService {
    @Override
    public String hex(String string) {
        return md5Hex(string);
    }
}
