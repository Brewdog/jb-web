package ru.demid.jbweb.web.service;

import lombok.Data;


@Data
public class CategoryDTO {
    private String name;
    private Long idCategory;
}
