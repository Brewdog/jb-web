package ru.demid.jbweb.web.service;

import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.demid.jbweb.web.api.converter.Converter;
import ru.demid.jbweb.web.entity.UserModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.repository.UserRepository;
import ru.demid.jbweb.web.security.UserRoles;

import javax.validation.Valid;
import java.util.Collections;


@Service
@AllArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final Converter<UserModel, UserDTO> userDtoConverter;

    public UserDTO auth(String email) {

        UserModel userModel = userRepository.findByEmail(email);
        if (userModel == null) {
            return null;
        }
        return userDtoConverter.convert(userModel);
    }

    public UserDTO registration(@Valid String email, @Valid String password, @Valid String name, @Valid String lastName) {

        try {
            UserModel userModel = new UserModel(email, passwordEncoder.encode(password), name, lastName);
            userModel.setUserRoles(Collections.singleton(UserRoles.USER));
            userRepository.saveAndFlush(userModel);
            return userDtoConverter.convert(userModel);

        } catch (DataIntegrityViolationException | NullPointerException e) {
            throw new ServiceException(e.getCause(), e.getMessage());
        }
    }

    public UserDTO getUserByID(Long id) {
        if (id == null || !userRepository.existsById(id)) {
            return null;
        }
        return userDtoConverter.convert(userRepository.getOne(id));
    }
}
