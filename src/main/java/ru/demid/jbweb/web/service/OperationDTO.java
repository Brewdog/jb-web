package ru.demid.jbweb.web.service;

import lombok.Data;
import ru.demid.jbweb.web.entity.Category;

import java.util.List;

@Data
public class OperationDTO {
    private long id;
    private long sum;
    private String sender;
    private String acceptor;
    private String date;
    private String comment;
    private List<Category> categories;

}
