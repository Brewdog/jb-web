package ru.demid.jbweb.web.service;

import org.springframework.stereotype.Component;

@Component
public interface DigestService {
    String hex(String string);
}
