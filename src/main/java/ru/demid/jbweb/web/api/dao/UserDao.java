//package ru.demid.jbweb.web.api.dao;
//
//import lombok.AllArgsConstructor;
//import org.springframework.stereotype.ru.demid.jbweb.web.Service;
//import ru.demid.jbweb.web.entity.UserModel;
//import ru.demid.jbweb.web.exception.CustomException;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityTransaction;
//import javax.persistence.NoResultException;
//import javax.persistence.TypedQuery;
//
//@ru.demid.jbweb.web.Service
//@AllArgsConstructor
//public class UserDao {
//    //    private final DataSource dataSource;
//    private final EntityManager em;
//
//    public UserModel findByEmailAndHash(String email, String hash) {
//        try {
//
//            TypedQuery<UserModel> userModelTypedQuery = em.createNamedQuery("UserModel.findByEmailAndHash", UserModel.class)
//                    .setParameter("email", email)
//                    .setParameter("password", hash);
//
//            return userModelTypedQuery.getSingleResult();
//
//        } catch (NoResultException e) {
//            throw new CustomException("User not found");
//        }
//    }
////    public UserModel findByEmailAndHash(String email, String hash) {
////        UserModel userModel = null;
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("select * from holder where email = ? and password = ?");
////            ps.setString(1, email);
////            ps.setString(2, hash);
////            ResultSet rs = ps.executeQuery();
////            if (rs.next()) {
////                userModel = new UserModel();
////                userModel.setId(rs.getLong("id"));
////                userModel.setEmail(rs.getString("email"));
////                userModel.setPassword(rs.getString("password"));
////                userModel.setName(rs.getString("first_name"));
////                userModel.setLastName(rs.getString("second_name"));
////
////            }
////        } catch (SQLException e) {
////            throw new CustomException(e);
////        }
////        return userModel;
////    }
//
//
//    public UserModel insert(String email, String hash, String name, String lastName) {
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//        UserModel newUser = new UserModel();
//        newUser.setEmail(email);
//        newUser.setPassword(hash);
//        newUser.setName(name);
//        newUser.setLastName(lastName);
//        em.persist(newUser);
//        transaction.commit();
//        return newUser;
//    }
//
////    public UserModel insert1(String email, String hash, String name, String lastName) {
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement(
////                    "insert into holder (email, password, first_name, second_name) values (?, ?, ?, ?)",
////                    Statement.RETURN_GENERATED_KEYS);
////            ps.setString(1, email);
////            ps.setString(2, hash);
////            ps.setString(3, name);
////            ps.setString(4, lastName);
////            ps.executeUpdate();
////            ResultSet rs = ps.getGeneratedKeys();
////            if (rs.next()) {
////                UserModel userModel = new UserModel();
////                userModel.setId(rs.getLong("id"));
////                userModel.setEmail(email);
////                userModel.setPassword(hash);
////                userModel.setName(name);
////                userModel.setLastName(lastName);
////                return userModel;
////            } else {
////                throw new CustomException("Cant generate new user");
////            }
////
////        } catch (SQLException e) {
////            throw new CustomException(e);
////        }
////    }
//}
