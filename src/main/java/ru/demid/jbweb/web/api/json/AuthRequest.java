package ru.demid.jbweb.web.api.json;

import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.Email;

@Data
public class AuthRequest {
    @NonNull
    @Email
    private String email;
    @NonNull
    private String password;
}
