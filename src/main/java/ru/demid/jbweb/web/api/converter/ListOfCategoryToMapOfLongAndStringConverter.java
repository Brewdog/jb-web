package ru.demid.jbweb.web.api.converter;

import org.springframework.stereotype.Component;
import ru.demid.jbweb.web.entity.Category;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ListOfCategoryToMapOfLongAndStringConverter implements Converter <List<Category>, Map<Long, String>> {
    @Override
    public Map<Long, String> convert(List<Category> list) {
        return list.stream().collect(Collectors.toMap(Category::getId, Category::getName));
    }
}
