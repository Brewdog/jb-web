package ru.demid.jbweb.web.api.json;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TrnsViewReq {
    @NotBlank
    private String startDate;

    @NotBlank
    private String finishDate;
}
