package ru.demid.jbweb.web.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.jbweb.web.entity.Category;

@Data
@AllArgsConstructor
public class CatDelRes {
    private Iterable<Category> result;
    private String message;
}
