package ru.demid.jbweb.web.api.json;

import lombok.Data;

@Data
public class AccDelReq {
    private Long idAcc;
}
