package ru.demid.jbweb.web.api.json;

import lombok.Data;

@Data
public class CatUpdReq {
    private String name;
    private Long idCat;
}
