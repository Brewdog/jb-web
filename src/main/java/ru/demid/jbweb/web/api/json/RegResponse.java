package ru.demid.jbweb.web.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegResponse {
    private String name;
    private String email;
    private Long userId;
}
