package ru.demid.jbweb.web.api.json;

import lombok.Data;

@Data
public class AccUpdReq {
    private Long idAcc;
    private String name;
    private Long balance;
}
