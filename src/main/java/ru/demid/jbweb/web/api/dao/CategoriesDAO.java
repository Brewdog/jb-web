//package ru.demid.jbweb.web.api.dao;
//
//import lombok.AllArgsConstructor;
//import org.springframework.stereotype.ru.demid.jbweb.web.Service;
//import ru.demid.newProject.entity.Category;
//import ru.demid.newProject.exception.CustomException;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityTransaction;
//import javax.persistence.PersistenceException;
//import javax.persistence.Query;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
//@ru.demid.jbweb.web.Service
//@AllArgsConstructor
//public class CategoriesDAO {
//    private EntityManager em;
//
//    public Map<Long, String> getAllCategories() {
//
//        List<Category> resultList = em.createNamedQuery("Category.findAll", Category.class).getResultList();
//        em.clear();
//
//        return resultList.stream().collect(Collectors.toMap(Category::getId, Category::getName));
//    }
//
//    public Category getCategoryById(long id) {
//        return em.find(Category.class, id);
//    }
////    public Map<Long, String> getCategories() {
////        Map <Long, String> categories = new HashMap<>();
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("SELECT * FROM category");
////            ResultSet rs = ps.executeQuery();
////            while (rs.next()) {
////                categories.put(rs.getLong("id"), rs.getString("type"));
////            }
////            rs.close();
////            ps.close();
////        } catch (SQLException e) {
////            throw new CustomException("Ошибка списка категорий", e);
////        }
////
////        return categories;
////    }
//
//    public String rename(long id, String name) {
//        if (em.find(Category.class, id) == null) {
//            return null;
//        }
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//        Query query = em.createNamedQuery("Category.rename")
//                .setParameter("name", name)
//                .setParameter("id", id);
//        query.executeUpdate();
//        em.flush();
//        em.clear();
//        transaction.commit();
//
//        return name;
//    }
//
//    public boolean remove(long id) {
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//
//        Query query = em.createNamedQuery("Category.remove")
//                .setParameter("id", id);
//        int i = query.executeUpdate();
//        em.flush();
//        em.clear();
//        transaction.commit();
//
//
//        return i == 1;
//    }
//
////    public String rename(long id, String name) {
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("update category set type = ? where id = ?");
////            ps.setString(1, name);
////            ps.setLong(2, id);
////            if (ps.executeUpdate() == 1) {
////                return name;
////            }
////        } catch (SQLException e) {
////            throw new CustomException("Ошибка переименования категории", e);
////        }
////        return null;
////    }
//
////    public boolean remove(long id) {
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("delete from category where id = ?");
////            ps.setLong(1, id);
////            return ps.executeUpdate() != 0;
////        } catch (SQLException e) {
////            throw new CustomException("Ошибка удаления категории", e);
////        }
////    }
//
//    public long addingCategory(String name) {
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//        Category category = new Category(name);
//        try {
//            em.persist(category);
//
//        } catch (PersistenceException e) {
//            throw new CustomException(e.getMessage());
//        }
//        transaction.commit();
//
//        return category.getId();
//    }
//
////    public long addingCategory(String name) {
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("insert into category (type) values (?)", Statement.RETURN_GENERATED_KEYS);
////            ps.setString(1, name);
////            ps.executeUpdate();
////            ResultSet rs = ps.getGeneratedKeys();
////            if (rs.next()) {
////                return rs.getLong("id");
////            }
////        } catch (SQLException e) {
////            throw new CustomException("SQL error with adding category", e);
////        }
////        return -1;
////    }
//}
