package ru.demid.jbweb.web.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.jbweb.web.entity.OperationModel;

@Data
@AllArgsConstructor
public class TrnsAddRes {
    private OperationModel result;
}
