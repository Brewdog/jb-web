package ru.demid.jbweb.web.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CatAddReq {
    @NotNull
    private String name;
}
