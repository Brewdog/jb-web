package ru.demid.jbweb.web.api.json;

import lombok.Data;

@Data
public class AccountAddRequest {
    private String name;
}
