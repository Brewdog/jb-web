package ru.demid.jbweb.web.api.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.demid.jbweb.web.api.json.*;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.AccountService;
import ru.demid.jbweb.web.service.UserDTO;

import java.util.List;

@Data
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class AccountController extends AbstractControllerForGetCurrentUser {
    private final AccountService accountService;

    @GetMapping("/accView")
    public @ResponseBody
    ResponseEntity<AccountsViewResponse> accView() {
        List<Account> list = accountService.viewAccounts(currentUser().getId());
        return ResponseEntity.ok(new AccountsViewResponse(list));
    }

    @PostMapping("/addAcc")
    public @ResponseBody
    ResponseEntity<AccountAddResponse> accAdd(@RequestBody AccountAddRequest accountAddRequest) {

        try {
            Account account = accountService.makeAccount(accountAddRequest.getName(), currentUser().getId());
            return ResponseEntity.ok(new AccountAddResponse(account.getName(), account.getBalance(), account.getId()));

        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
    }

    @PostMapping("/accDel")
    public @ResponseBody
    ResponseEntity<AccDelRes> accDel(@RequestBody AccDelReq accDelReq) {
        UserDTO user = currentUser();
        if (accountService.delAccount(accDelReq.getIdAcc(), user.getId())) {
            return ResponseEntity.ok(new AccDelRes(accountService.viewAccounts(user.getId()), "Account has been deleted successfully"));
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @PostMapping("/accUpd")
    public @ResponseBody
    ResponseEntity<AccUpdRes> accUpd(@RequestBody AccUpdReq accUpdReq) {
        UserDTO user = currentUser();
        try {
            if (accountService.updateInfo(user.getId(), accUpdReq.getIdAcc(), accUpdReq.getName(), accUpdReq.getBalance()) != null) {
                return ResponseEntity.ok(new AccUpdRes(accountService.viewAccounts(user.getId())));
            } else {
                return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
        }
    }
}
