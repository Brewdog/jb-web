package ru.demid.jbweb.web.api.json;

import lombok.Data;
import ru.demid.jbweb.web.entity.Category;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class TrnsAddReq {
    private Long idSenderAcc;
    private Long idAccepterAcc;
    @NotBlank
    private Long summ;
    @NotBlank
    private String date;
    private String comment;
    private List<Category> categories;
}
