//package ru.demid.jbweb.web.api.dao;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.ru.demid.jbweb.web.Service;
//import ru.demid.jbweb.web.entity.Account;
//import ru.demid.jbweb.web.entity.OperationModel;
//import ru.demid.jbweb.web.entity.Category;
//import ru.demid.jbweb.web.exception.CustomException;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityTransaction;
//import java.util.Collections;
//import java.util.List;
//
//@ru.demid.jbweb.web.Service
//@RequiredArgsConstructor
//public class OperationDAO {
//    private final EntityManager em;
//
//    public List<OperationModel> reportByDate(long idHolder, String dateStart, String dateFinish) {
//        return em.createNamedQuery("Operation.GetByDateAndCategories", OperationModel.class)
//                .setParameter("start", dateStart)
//                .setParameter("finish", dateFinish)
//                .setParameter("id", idHolder)
//                .getResultList();
//    }
//
////    public List<OperationModel> reportByDate(long idHolder, String dateStart, String dateFinish) {
////        OperationModel opm;
////        List<OperationModel> list = new ArrayList<>();
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("select sum(o.sum) as Summs, count(o.sum) as Transaction, c.type, c.id from operation o\n" +
////                    "    join category_to_operation cto on o.id = cto.operation_id\n" +
////                    "    join category c on cto.category_id = c.id\n" +
////                    "    join account a on o.sender = a.id or o.acceptor = a.id\n" +
////                    "    where a.holder_id = ? and to_date(o.date, 'dd-MM-YYYY') between to_date(?,'dd-MM-YYYY') and to_date(?,'dd-MM-YYYY') group by c.type, c.type, c.id");
////            ps.setLong(1, idHolder);
////            ps.setString(2, dateStart);
////            ps.setString(3, dateFinish);
////            ResultSet rs = ps.executeQuery();
////            while (rs.next()) {
////                opm = new OperationModel();
////                opm.setTransaction(rs.getLong("Transaction"));
////                opm.setSum(rs.getLong("Summs"));
////                opm.setCategories(Arrays.asList(rs.getLong("id")));
////                list.add(opm);
////            }
////        } catch (SQLException e) {
////            throw new CustomException("Error getting data from DB", e);
////        }
////        return list;
////    }
//
////    public boolean connectOperationToCategories(Long idOpr, Long idCat) {
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("insert into category_to_operation(category_id, operation_id) VALUES (?,?)");
////            ps.setLong(1, idCat);
////            ps.setLong(2, idOpr);
////            return ps.executeUpdate() == 1;
////
////        } catch (SQLException e) {
////            throw new CustomException("Error adding transaction to DB", e);
////        }
////    }
//
//
//    public OperationModel addOperation(long idHolder, Long idSenderAccount, Long idAccepterAccount, long sum, String date, String comment, List<Category> categories) {
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//        OperationModel resultOperation = new OperationModel();
//        if (idSenderAccount != null) {
//            Account accountSender = reduceBalance(idSenderAccount, sum);
//            if (accountSender.getAccountHolder().getId() != (idHolder)) {
//                transaction.rollback();
//                return null;
//            } else {
//                resultOperation.setSender(accountSender);
//            }
//        }
//        if (idAccepterAccount != null) {
//            Account accountAcceptor = increaseBalance(idAccepterAccount, sum);
//            if (accountAcceptor.getAccountHolder().getId() != (idHolder)) {
//                transaction.rollback();
//                return null;
//            } else {
//                resultOperation.setAcceptor(accountAcceptor);
//            }
//        }
//        if (resultOperation.getSender() == null && resultOperation.getAcceptor() == null) {
//            transaction.rollback();
//            return null;
//        } else {
//            resultOperation.setSum(sum);
//            resultOperation.setDate(date);
//            resultOperation.setComment(comment);
//            if (categories == null || categories.isEmpty()) {
//                resultOperation.setCategories(Collections.singletonList(em.find(Category.class, 6L)));
//            } else {
//                resultOperation.setCategories(categories);
//            }
//            em.persist(resultOperation);
//            em.flush();
//            transaction.commit();
//            return resultOperation;
//        }
//    }
//
//    private Account reduceBalance(Long id, Long sum) {
//        Account account = em.find(Account.class, id);
//        if (account == null) {
//            throw new CustomException("User not found");
//        } else {
//            account.setBalance(account.getBalance() - sum);
//            em.persist(account);
//            return account;
//        }
//    }
//
//    private Account increaseBalance(Long id, Long sum) {
//        Account account = em.find(Account.class, id);
//        if (account == null) {
//            throw new CustomException("User not found");
//        } else {
//            account.setBalance(account.getBalance() + sum);
//            em.persist(account);
//            return account;
//        }
//    }
//
//    public boolean removeOperationById(Long operationId, Long holderId) {
//        OperationModel resultOm = em.createNamedQuery("Operation.findOperationById", OperationModel.class)
//                .setParameter("idOp", operationId)
//                .setParameter("idHld", holderId)
//                .getSingleResult();
//        if (resultOm == null) {
//            return false;
//        }
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
//        em.remove(resultOm);
//        em.flush();
//        transaction.commit();
//        return true;
//    }
////    public OperationModel addOperation(long idHolder, Long idSenderAccount, Long idAccepterAccount, long sum, String date, String comment) {
////        try (Connection conn = dataSource.getConnection()) {
////            conn.setAutoCommit(false);
////            boolean accepter = false;
////            boolean sender = false;
////
////            try {
////                if (idSenderAccount != null) {
////                    PreparedStatement psWithdraw = conn.prepareStatement("update account set balance = balance - ? where holder_id = ? and id = ?");
////                    psWithdraw.setLong(1, sum);
////                    psWithdraw.setLong(2, idHolder);
////                    psWithdraw.setLong(3, idSenderAccount);
////                    if (psWithdraw.executeUpdate() != 1) {
////                        conn.rollback();
////                        throw new CustomException("Error with transaction of withdrawing");
////                    }
////                    sender = true;
////                }
////                if (idAccepterAccount != null) {
////                    PreparedStatement psDepo = conn.prepareStatement("update account set balance = balance + ? where holder_id = ? and id = ?");
////                    psDepo.setLong(1, sum);
////                    psDepo.setLong(2, idHolder);
////                    psDepo.setLong(3, idAccepterAccount);
////                    if (psDepo.executeUpdate() != 1) {
////                        conn.rollback();
////                        throw new CustomException("Error with transaction of depositing");
////                    }
////                    accepter = true;
////                }
////
////                if (sender || accepter) {
////                    PreparedStatement createOperation = conn.prepareStatement("insert into operation (sum, sender, acceptor, date, comment) VALUES (?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
////                    createOperation.setLong(1, sum);
////                    if (idSenderAccount != null) {
////                        createOperation.setLong(2, idSenderAccount);
////                    } else {
////                        createOperation.setNull(2, Types.INTEGER);
////                    }
////
////                    if (idAccepterAccount != null) {
////                        createOperation.setLong(3, idAccepterAccount);
////                    } else {
////                        createOperation.setNull(3, Types.INTEGER);
////                    }
////                    createOperation.setString(4, date);
////                    createOperation.setString(5, comment);
////                    createOperation.executeUpdate();
////                    ResultSet rs = createOperation.getGeneratedKeys();
////                    if (rs.next()) {
////                        OperationModel opm = new OperationModel();
////                        opm.setId(rs.getLong("id"));
////                        opm.setSum(sum);
////                        if (idAccepterAccount != null) {
////                            opm.setAcceptor(idAccepterAccount);
////                        }
////                        if (idSenderAccount != null) {
////                            opm.setSender(idSenderAccount);
////                        }
////                        opm.setDate(date);
////                        if (comment != null) {
////                            opm.setComment(comment);
////                        }
////                        conn.commit();
////                        return opm;
////                    }
////                }
////
////
////            } catch (SQLException e) {
////                conn.rollback();
////                throw new CustomException("Error with adding transaction", e);
////            } finally {
////                conn.setAutoCommit(true);
////            }
////
////        } catch (SQLException e) {
////            throw new CustomException("Error adding transaction to DB", e);
////        }
////        return null;
////    }
//
//}
