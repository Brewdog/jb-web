package ru.demid.jbweb.web.api.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.demid.jbweb.web.api.converter.Converter;
import ru.demid.jbweb.web.api.json.*;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.CategoriesService;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class CategoriesController extends AbstractControllerForGetCurrentUser {
    private final CategoriesService service;
    private final Converter<List<Category>, Map<Long, String>> converter;

    @GetMapping("/catView")
    public @ResponseBody
    ResponseEntity<CatViewRes> catView() {
        try {
            Map<Long, String> view = converter.convert(service.view());
            return ResponseEntity.ok(new CatViewRes(view));

        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @PostMapping("/catAdd")
    public @ResponseBody
    ResponseEntity<CatAddRes> catAdd(@RequestBody CatAddReq catAddReq) {
        try {
            if (service.addCategories(catAddReq.getName()) > 0) {
                return ResponseEntity.ok(new CatAddRes(service.view(), "Category has been added successfully"));
            } else {
                return ResponseEntity.noContent().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.unprocessableEntity().body(new CatAddRes(Collections.emptyList(), e.getMessage()));
        }
    }

    @PostMapping("/catUpd")
    public @ResponseBody
    ResponseEntity<CatUpdRes> catUpd(@RequestBody CatUpdReq catUpdReq) {
        try {
            if (service.setCategories(catUpdReq.getIdCat(), catUpdReq.getName()) != null) {
                return ResponseEntity.ok(new CatUpdRes(converter.convert(service.view()), "Information has been updated"));
            }
            return ResponseEntity.noContent().build();

        } catch (ServiceException e) {
            return ResponseEntity.unprocessableEntity().body(new CatUpdRes(Collections.emptyMap(), e.getMessage()));
        }
    }

    @PostMapping("/catDel")
    public @ResponseBody
    ResponseEntity<CatDelRes> catDel(@RequestBody CatDelReq catDelReq) {
        try {
            if (service.delCategories(catDelReq.getIdCat())) {
                return ResponseEntity.ok(new CatDelRes(service.view(), "Category has been deleted successfully"));
            }
            return ResponseEntity.noContent().build();

        } catch (ServiceException e) {
            return ResponseEntity.unprocessableEntity().body(new CatDelRes(Collections.emptyList(), e.getMessage()));
        }
    }
}
