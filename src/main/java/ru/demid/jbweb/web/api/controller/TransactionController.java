package ru.demid.jbweb.web.api.controller;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.demid.jbweb.web.api.json.TrnsAddReq;
import ru.demid.jbweb.web.api.json.TrnsAddRes;
import ru.demid.jbweb.web.api.json.TrnsViewReq;
import ru.demid.jbweb.web.api.json.TrnsViewRes;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.OperationService;
import ru.demid.jbweb.web.service.UserDTO;

import javax.validation.Valid;
import java.util.List;

@Data
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class TransactionController extends AbstractControllerForGetCurrentUser {
    private final OperationService service;

    @PostMapping("/transAdd")
    @Transactional
    public @ResponseBody
    ResponseEntity<?> trnsAdd(@RequestBody TrnsAddReq trnsAddReq) {
        UserDTO user = currentUser();
        try {
            OperationModel operation = service.addNewOperation(user.getId(),
                    trnsAddReq.getIdSenderAcc(),
                    trnsAddReq.getIdAccepterAcc(),
                    trnsAddReq.getSumm(),
                    trnsAddReq.getDate(),
                    trnsAddReq.getComment(),
                    trnsAddReq.getCategories()
            );

            return ResponseEntity.ok(new TrnsAddRes(operation));

        } catch (ServiceException e) {
            return ResponseEntity.noContent().eTag(e.getMessage()).build();
        }
    }

    @PostMapping("/transView")
    public @ResponseBody
    ResponseEntity<?> trnsView(@RequestBody @Valid TrnsViewReq trnViewReq) {

        try {
            List<OperationModel> transactions = service.viewByCategoryAndDate(currentUser().getId(), trnViewReq.getStartDate(), trnViewReq.getFinishDate());
            return ResponseEntity.ok(new TrnsViewRes(transactions));
        } catch (ServiceException e) {
            return ResponseEntity.notFound().varyBy(e.getMessage()).build();
        }
    }
}
