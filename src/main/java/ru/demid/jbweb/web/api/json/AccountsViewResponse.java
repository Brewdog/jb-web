package ru.demid.jbweb.web.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.jbweb.web.entity.Account;

import java.util.List;

@Data
@AllArgsConstructor
public class AccountsViewResponse {
    private List <Account> accounts;
}
