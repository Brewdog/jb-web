package ru.demid.jbweb.web.api.json;

import lombok.Data;

@Data
public class CatDelReq {
    private Long idCat;
}
