package ru.demid.jbweb.web.api.converter;

import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StringToDateConverter implements Converter <String, Date> {
    @SneakyThrows
    @Override
    public Date convert(String s) {
        return new SimpleDateFormat("dd-MM-yyyy").parse(s);
    }
}
