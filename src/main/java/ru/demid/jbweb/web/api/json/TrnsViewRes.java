package ru.demid.jbweb.web.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.jbweb.web.entity.OperationModel;

import java.util.List;

@Data
@AllArgsConstructor
public class TrnsViewRes {
    private List<OperationModel> transactions;
}
