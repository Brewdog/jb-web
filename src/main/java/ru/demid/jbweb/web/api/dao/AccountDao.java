//package ru.demid.jbweb.web.api.dao;
//
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.ru.demid.jbweb.web.Service;
//import ru.demid.jbweb.web.entity.Account;
//import ru.demid.jbweb.web.entity.UserModel;
//import ru.demid.jbweb.web.exception.CustomException;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityTransaction;
//import java.util.List;
//
//@ru.demid.jbweb.web.Service
//@RequiredArgsConstructor
//public class AccountDao {
//    private final EntityManager em;
//
//    public List<Account> getAccounts(long id) {
//        return em.createNamedQuery("Account.findAll", Account.class)
//                .setParameter("idHolder", id)
//                .getResultList();
//    }
////    public List<Account> getAccounts(long id) {
////        List<Account> list = new ArrayList<>();
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("select * from account where holder_id = ?");
////            ps.setLong(1, id);
////            ResultSet rs = ps.executeQuery();
////            makeList(id, list, ps, rs);
////        } catch (SQLException e) {
////
////            throw new CustomException("Ошибка поиска счетов", e);
////        }
////        return list;
////    }
//
//    public Account setAccounts(long holderID, String name) {
//        EntityTransaction transaction = em.getTransaction();
//
//        UserModel userModel = em.find(UserModel.class, holderID);
//        if (userModel == null) {
//            throw new CustomException("User not found");
//        }
//        transaction.begin();
//        Account account = new Account();
//        account.setAccountHolder(userModel);
//        account.setName(name);
//        account.setBalance(0);
//
//        em.persist(account);
//        transaction.commit();
//        em.clear();
//
//        return account;
//    }
//
////    public Account setAccounts(long holderID, String name) {
////        Account account = null;
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("insert into account (name, balance, holder_id) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
////            ps.setString(1, name);
////            ps.setFloat(2, 0.0f);
////            ps.setLong(3, holderID);
////            ps.executeUpdate();
////            ResultSet rs = ps.getGeneratedKeys();
////            if (rs.next()) {
////                account = new Account();
////                account.setId(rs.getLong("id"));
////                account.setName(name);
////                account.setBalance(0);
////                return account;
////            }
////
////        } catch (SQLException e) {
////            throw new CustomException("Ошибка создания счета", e);
////        }
////        return account;
////    }
//
//    public int setAccounts(long holderID, long id) {
//
//        UserModel userModel = em.find(UserModel.class, holderID);
//        if (userModel == null) {
//            return 0;
//        }
//        Account account = em.find(Account.class, id);
//        if (account == null) {
//            return 0;
//        }
//        em.getTransaction().begin();
//        em.remove(account);
//        em.getTransaction().commit();
//
//        return 1;
//    }
//
////    public int setAccounts1(long holderID, long id) {
////        int result;
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("delete from account WHERE holder_id = ? and id = ?");
////            ps.setLong(1, holderID);
////            ps.setLong(2, id);
////            result = ps.executeUpdate();
////
////        } catch (SQLException e) {
////            throw new CustomException("Ошибка удаления счета", e);
////        }
////        return result;
////    }
//
//    public long viewBalance(long idHolder, long idAccount) {
//
//        UserModel userModel = em.find(UserModel.class, idHolder);
//        if (userModel == null) {
//            return -1;
//        }
//
//        Account account = em.find(Account.class, idAccount);
//        if (account == null || !account.getAccountHolder().equals(userModel)) {
//            return -1;
//        }
//        return (long) account.getBalance();
//    }
//
////    public long viewBalance1(long idHolder, long idAccount) {
////        try (Connection conn = dataSource.getConnection()) {
////
////            PreparedStatement ps = conn.prepareStatement("SELECT balance FROM account WHERE id = ? and holder_id = ?");
////            ps.setLong(1, idAccount);
////            ps.setLong(2, idHolder);
////
////            ResultSet rs = ps.executeQuery();
////            if (rs.next()) {
////                return rs.getLong("balance");
////            }
////            return -1;
////
////        } catch (SQLException e) {
////            System.out.println(e.getMessage());
////            throw new CustomException("Error getting balance", e);
////        }
////    }
//
////    private void makeList(long id, List<Account> list, PreparedStatement ps, ResultSet rs) throws SQLException {
////        while (rs.next()) {
////            Account accModel = new Account();
////            accModel.setId(rs.getLong("id"));
////            accModel.setBalance(rs.getLong("balance"));
////            accModel.setName(rs.getString("name"));
////            list.add(accModel);
////        }
////        rs.close();
////        ps.close();
////    }
//
//
//    public boolean changeInformation(long holderId, long accId, String name, float balance) {
//
//        Account account = em.find(Account.class, accId);
//        if (account == null || account.getAccountHolder().getId() != holderId) {
//            return false;
//        }
//        em.getTransaction().begin();
//        account.setName(name);
//        account.setBalance(balance);
//        em.persist(account);
//        em.getTransaction().commit();
//
//        return true;
//    }
////
////    public boolean changeInformation1(long holderId, long accId, String name, float balance) {
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("update account set name = ?, balance = ? where id = ? and holder_id = ?");
////            ps.setString(1, name);
////            ps.setFloat(2, balance);
////            ps.setLong(3, accId);
////            ps.setLong(4, holderId);
////            if (ps.executeUpdate() == 0) {
////                return false;
////            } else {
////                return true;
////            }
////        } catch (SQLException e) {
////            throw new CustomException("Error of changing information", e);
////        }
////    }
//
//    public boolean changeInformation(long holderId, long accId, String name) {
//
//        Account account = em.find(Account.class, accId);
//        if (account == null || account.getAccountHolder().getId() != holderId) {
//            return false;
//        }
//        em.getTransaction().begin();
//        account.setName(name);
//        em.persist(account);
//        em.getTransaction().commit();
//
//        return true;
//    }
////    public boolean changeInformation1(long holderId, long accId, String name) {
////        try (Connection conn = dataSource.getConnection()) {
////            PreparedStatement ps = conn.prepareStatement("update account set name = ? where id = ? and holder_id = ?");
////            ps.setString(1, name);
////            ps.setLong(2, accId);
////            ps.setLong(3, holderId);
////            return ps.executeUpdate() != 0;
////
////        } catch (SQLException e) {
////            throw new CustomException("Error of changing information", e);
////        }
////    }
//}
