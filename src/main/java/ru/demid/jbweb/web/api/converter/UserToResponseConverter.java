package ru.demid.jbweb.web.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.demid.jbweb.web.api.json.AuthResponse;
import ru.demid.jbweb.web.service.UserDTO;

@Component
public class UserToResponseConverter implements Converter <UserDTO, AuthResponse> {
    @Override
    public AuthResponse convert(UserDTO user) {
        return new AuthResponse(user.getId(), user.getEmail());
    }
}
