package ru.demid.jbweb.web.api.converter;

import org.springframework.stereotype.Component;
import ru.demid.jbweb.web.entity.UserModel;
import ru.demid.jbweb.web.service.UserDTO;


@Component
public class UserModelToUserDtoConverter implements Converter<UserModel, UserDTO> {

    @Override
    public UserDTO convert(UserModel userModel) {
        return new UserDTO(
                userModel.getId(),
                userModel.getEmail(),
                userModel.getName(),
                userModel.getLastName(),
                userModel.getAccounts());
    }
}
