package ru.demid.jbweb.web.api.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import ru.demid.jbweb.web.entity.Account;

import java.util.List;

@Data
@AllArgsConstructor
public class AccUpdRes {
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<Account> accounts;
}
