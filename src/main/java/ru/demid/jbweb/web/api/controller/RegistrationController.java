package ru.demid.jbweb.web.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.demid.jbweb.web.api.json.RegRequst;
import ru.demid.jbweb.web.api.json.RegResponse;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class RegistrationController {
    private final AuthService authService;

    @PostMapping("/reg")
    public @ResponseBody
    ResponseEntity<?> registration(@RequestBody RegRequst regRequst) {
        try {
            UserDTO userDTO = authService.registration(regRequst.getEmail(), regRequst.getPassword(), regRequst.getName(), regRequst.getLastName());
            return ok(new RegResponse(userDTO.getName(), userDTO.getEmail(), userDTO.getId()));

        } catch (ServiceException e) {
            return ResponseEntity.unprocessableEntity().body(e.getMessage());
        }
    }

}
