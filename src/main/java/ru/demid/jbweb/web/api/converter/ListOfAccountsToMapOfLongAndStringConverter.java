package ru.demid.jbweb.web.api.converter;

import org.springframework.stereotype.Component;
import ru.demid.jbweb.web.entity.Account;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ListOfAccountsToMapOfLongAndStringConverter implements Converter <List<Account>, Map<Long, String>> {
    @Override
    public Map<Long, String> convert(List<Account> list) {
        return list.stream().collect(Collectors.toMap(Account::getId, Account::getName));
    }
}
