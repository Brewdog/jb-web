package ru.demid.jbweb.web.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.UserDTO;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = {"/", "/web"})
public class LoginWebController extends AbstractControllerForGetCurrentUser {

    @GetMapping("/")
    public String index() {
        UserDTO user = currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        return "redirect:/home";
    }

    @GetMapping("/login-form")
    public String getLogin() {
        return "login-form";
    }

    @GetMapping("/home")
    public String home(Model model) {
        UserDTO user = currentUser();
        model.addAttribute("name", user.getName());
        return "home";
    }
}
