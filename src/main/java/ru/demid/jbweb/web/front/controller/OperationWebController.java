package ru.demid.jbweb.web.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.demid.jbweb.web.entity.Category;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.front.form.AddTransactionForm;
import ru.demid.jbweb.web.front.form.ViewOperationForm;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.CategoriesService;
import ru.demid.jbweb.web.service.OperationService;
import ru.demid.jbweb.web.service.UserDTO;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class OperationWebController extends AbstractControllerForGetCurrentUser {
    private final OperationService service;
    private final CategoriesService categoriesService;
    private final String REGEX = "^(?:(?:31([-])(?:0?[13578]|1[02]))\\1|(?:(?:29|30)([-])(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29([-])0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])([-])(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";


    @GetMapping("/viewTrans")
    public String getViewOperations(Model model) {
        UserDTO user = currentUser();
        model.addAttribute("name", user.getName())
                .addAttribute("form", new ViewOperationForm())
                .addAttribute("operations", new ArrayList<OperationModel>());
        return "trnsView";
    }

    @PostMapping("/viewTrans")
    public String postViewOperations(Model model,
                                     @ModelAttribute("form") @Valid ViewOperationForm form,
                                     BindingResult result) {

        UserDTO user = currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (form.getFinishDate().isEmpty()) {
            form.setFinishDate(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));
        }
        if (form.getStartDate().isEmpty()) {
            form.setStartDate(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));
        }
        if (!form.getStartDate().matches(REGEX)
        ) {
            result.addError(new FieldError("form", "startDate", "Wrong date format"));
        }
        if (!form.getFinishDate().matches(REGEX)
        ) {
            result.addError(new FieldError("form", "finishDate", "Wrong date format"));
        }

        if (!result.hasErrors()) {
            List<OperationModel> operations = service.viewByCategoryAndDate(user.getId(), form.getStartDate(), form.getFinishDate());
            if (operations != null) {
                model.addAttribute("name", user.getName())
                        .addAttribute("operations", operations)
                        .addAttribute("form", form);

                return "trnsView";
            }
            result.addError(new FieldError("form", "startDate", "Something went wrong"));
        }
        model.addAttribute("form", form)
                .addAttribute("name", user.getName())
                .addAttribute("operations", new ArrayList<>());
        return "trnsView";
    }

    @GetMapping("/addTrans")
    public String getAddTransaction(Model model) {
        UserDTO user = currentUser();
        List<Category> categories = categoriesService.view();
        model.addAttribute("form", new AddTransactionForm())
                .addAttribute("accounts", user.getAccounts())
                .addAttribute("categories", categories);
        return "trnsAdd";
    }

    @PostMapping("/addTrans")
    public String postAddTransaction(Model model,
                                     @ModelAttribute("form") @Valid AddTransactionForm form,
                                     BindingResult result) {
        UserDTO user = currentUser();
        if (user == null) {
            return "redirect:/login-form";
        }
        if (form.getDate().isEmpty()) {
            form.setDate(new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime()));
        }
        if (!form.getDate().matches(REGEX)) {
            result.addError(new FieldError("form", "Date", "Format of date must be dd-mm-yyyy"));
        }
        if (!result.hasErrors()) {
            try {
                OperationModel operation = makeTransactionFromForm(form, user);
                if (operation != null) {
                    model.addAttribute("operations", Collections.singletonList(operation))
                            .addAttribute("form", new ViewOperationForm());
                    return "trnsView";
                }

            } catch (ServiceException e) {
                result.addError(new FieldError("form", "sender", e.getMessage()));
            }
            result.addError(new FieldError("form", "sender", "Cant make the transaction"));
        }
        model.addAttribute("form", form);
        return "trnsAdd";
    }

    private OperationModel makeTransactionFromForm(@ModelAttribute("form") @Valid AddTransactionForm form, UserDTO user) throws ServiceException {
        return service.addNewOperation(user.getId(),
                form.getSenderAcc() == null ? null : form.getSenderAcc().getId(),
                form.getAccepterAcc() == null ? null : form.getAccepterAcc().getId(),
                form.getSum(),
                form.getDate(),
                form.getComment(),
                form.getCategories());
    }
}
