package ru.demid.jbweb.web.front.form;

import lombok.Data;

@Data
public class ViewOperationForm {
    private String startDate;
    private String finishDate;
}
