package ru.demid.jbweb.web.front.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UpdAccForm {
    @NotNull
    private Long idAcc;

    @NotEmpty
    private String name;

    private Long balance;
}
