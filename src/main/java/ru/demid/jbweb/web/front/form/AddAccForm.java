package ru.demid.jbweb.web.front.form;

import lombok.Data;

@Data
public class AddAccForm {
    private String name;
}
