package ru.demid.jbweb.web.front.form;

import lombok.Data;

@Data
public class ViewCategoriesForm {
    private Long idCat;
    private String name;
}
