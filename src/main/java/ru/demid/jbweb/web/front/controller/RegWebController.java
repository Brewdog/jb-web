package ru.demid.jbweb.web.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.front.form.RegistrationForm;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class RegWebController extends AbstractControllerForGetCurrentUser {
    private final AuthService authService;

    @GetMapping("/registration")
    public String getRegistration(Model model) {
        if (model.getAttribute("form") == null) {
            model.addAttribute("form", new RegistrationForm());
        }
        return "/registration";
    }

    @PostMapping("/registration")
    public String postRegistration(@ModelAttribute("form") @Valid RegistrationForm form,
                                   BindingResult result,
                                   Model model) {
        try {
            if (!result.hasErrors()) {
                UserDTO newUser = authService.registration(form.getEmail(),
                        form.getPassword(),
                        form.getName(),
                        form.getLastName());
                if (newUser != null) {
                    return "redirect:/";
                }
                result.addError(new FieldError("form", "email", "Something went wrong. Try again"));
            }
        } catch (ServiceException e) {
            result.addError(new FieldError("form", "email", e.getMessage()));
        }
        model.addAttribute("form", form);
        return "/registration";
    }
}
