package ru.demid.jbweb.web.front.form;

import lombok.Data;
import org.springframework.lang.Nullable;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.entity.Category;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AddTransactionForm {
    @Nullable
    private Account senderAcc;
    @Nullable
    private Account accepterAcc;
    @NotNull
    private Long sum;
    private String date;
    private String comment;
    private List<Category> categories;
}
