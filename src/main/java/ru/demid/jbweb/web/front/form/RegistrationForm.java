package ru.demid.jbweb.web.front.form;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class RegistrationForm {

    @NotEmpty
    @Size(min = 3, message = "min 3 symbols")
    private String name;

    @NotEmpty
    private String lastName;

    @Email
    @NotEmpty
    private String email;

    @NotEmpty
    @Size(min = 5, message = "min 5 symbols")
    private String password;
}
