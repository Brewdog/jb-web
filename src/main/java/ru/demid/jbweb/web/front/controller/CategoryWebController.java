package ru.demid.jbweb.web.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.demid.jbweb.web.front.form.ViewCategoriesForm;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.CategoriesService;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class CategoryWebController extends AbstractControllerForGetCurrentUser {
    private final CategoriesService service;

    @GetMapping("/viewCat")
    public String getCategories(Model model) {
        if (currentUser() == null) {
            return "redirect:/login-form";
        }
        model.addAttribute("categories", service.view());
        return "catView";
    }

    @PostMapping("/viewCat")
    public String postCategories(@ModelAttribute("form") @Valid ViewCategoriesForm form,
                                 BindingResult result,
                                 Model model) {
        if (currentUser() == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            Long action = form.getIdCat();
            String name = form.getName();
            if (action < 0) {
                if (service.delCategories(action * -1)) {
                    model.addAttribute("categories", service.view());
                    return "catView";
                }
                result.addError(new FieldError("form", "action", "Can't remove category"));

            } else {
                if (!name.isEmpty()) {
                    if (service.setCategories(action, name) != null) {
                        model.addAttribute("categories", service.view());
                        return "catView";
                    }
                    result.addError(new FieldError("form", "action", "Can't update category"));
                } else {
                    result.addError(new FieldError("form", "name", "Need name to update category"));
                }
            }
        }
        model.addAttribute("form", form)
                .addAttribute("categories", service.view());
        return "catView";
    }

    @PostMapping("/addCat")
    public String postAddCategory(@ModelAttribute("form") @Valid ViewCategoriesForm form,
                                  BindingResult result,
                                  Model model) {
        if (currentUser() == null) {
            return "redirect:/login-form";
        }
        if (!result.hasErrors()) {
            String name = form.getName();
            if (!name.isEmpty()) {
                if (service.addCategories(name) > 0) {
                    model.addAttribute("categories", service.view());
                    return "catView";
                } else {
                    result.addError(new FieldError("form", "action", "Something went wrong"));
                }
            } else {
                result.addError(new FieldError("form", "name", "Name cant be empty"));
            }
        }
        model.addAttribute("form", form)
                .addAttribute("categories", service.view());
        return "catView";
    }
}
