package ru.demid.jbweb.web.front.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.front.form.AddAccForm;
import ru.demid.jbweb.web.front.form.DelAccForm;
import ru.demid.jbweb.web.front.form.UpdAccForm;
import ru.demid.jbweb.web.security.AbstractControllerForGetCurrentUser;
import ru.demid.jbweb.web.service.AccountService;
import ru.demid.jbweb.web.service.UserDTO;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class AccountWebController extends AbstractControllerForGetCurrentUser {

    private final AccountService accountService;

    @GetMapping("/accounts")
    public String getAccounts(Model model) {
        UserDTO user = currentUser();
        model.addAttribute("name", user.getName());
        model.addAttribute("accounts", user.getAccounts());
        return "accounts";
    }

    @GetMapping("/updAcc")
    public String getUpdAccounts(Model model) {
        UserDTO user = currentUser();
        model.addAttribute("form", new UpdAccForm())
                .addAttribute("name", user.getName())
                .addAttribute("accounts", user.getAccounts());
        return "accUpd";
    }

    @PostMapping("/updAcc")
    public String postUpdAccounts(Model model,
                                  @ModelAttribute("form") @Valid UpdAccForm form,
                                  BindingResult result,
                                  RedirectAttributes redirectAttributes) {
        UserDTO user = currentUser();
        if (user.getAccounts().stream().anyMatch(account -> account.getName().equals(form.getName()))) {
            result.addError(new FieldError("form", "name", "Name already exist"));
        }
        if (!result.hasErrors()) {
            Account account = accountService.updateInfo(user.getId(), form.getIdAcc(), form.getName(), form.getBalance());
            if (account != null) {
                redirectAttributes.addAttribute("newAccountName", account.getName());
                return "redirect:/accounts";
            } else {
                result.addError(new FieldError("form", "name", "Something went wrong"));
            }
        }
        model.addAttribute("form", form)
                .addAttribute("name", user.getName())
                .addAttribute("accounts", accountService.viewAccounts(user.getId()));
        return "accUpd";
    }

    @GetMapping("/addAcc")
    public String getAddAccount(Model model) {
        currentUser();
        model.addAttribute("form", new AddAccForm());
        return "accAdd";
    }

    @PostMapping("/addAcc")
    public String postAddAccount(Model model,
                                 @ModelAttribute("form") @Valid AddAccForm form,
                                 BindingResult result,
                                RedirectAttributes attributes) {
        UserDTO user = currentUser();
        if (user.getAccounts().stream().anyMatch(account -> account.getName().equals(form.getName()))) {
            result.addError(new FieldError("form", "name", "Name already exist"));
        }
        if (!result.hasErrors()) {
            Account account = accountService.makeAccount(form.getName(), user.getId());
            if (account != null) {
                attributes.addAttribute("name", user.getName());
                return "redirect:/accounts";
            }
            result.addError(new FieldError("form", "name", "Cannot make new account"));
        }
        model.addAttribute("form", form)
                .addAttribute("name", user.getName());
        return "accAdd";
    }

    @GetMapping("/delAcc")
    public String getDeleteAccount(Model model) {
        UserDTO user = currentUser();
        List<Account> accounts = accountService.viewAccounts(user.getId());
        if (!accounts.isEmpty()) {
            model.addAttribute("accounts", accounts)
                    .addAttribute("name", user.getName());
            return "accDel";
        }
        return "redirect:/accounts";
    }

    @PostMapping("/delAcc")
    public String postDeleteAccount(Model model,
                                    @ModelAttribute("form") @Valid DelAccForm form,
                                    BindingResult result) {
        UserDTO user = currentUser();
        if (!result.hasErrors()){
                if (accountService.delAccount(form.getIdAcc(), user.getId())) {
                    model.addAttribute("name", user.getName());
                    return "redirect:/accounts";
                }
        }
        result.addError(new FieldError("form", "name", "Something went wrong"));
        model.addAttribute("form", form);
        return "accDel";
    }
}
