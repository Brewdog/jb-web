package ru.demid.jbweb.web.console;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.demid.jbweb.web.console.menu.Menu;
import ru.demid.jbweb.web.console.menu.MenuEntry;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.entity.OperationModel;
import ru.demid.jbweb.web.exception.ServiceException;
import ru.demid.jbweb.web.service.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

@Component
@Data
@RequiredArgsConstructor
public class ConsoleRunner {
    static UserDTO userDTO;
    private final AuthService authService;
    private final CategoriesService ctgSrvs;
    private final AccountService accountService;
    private final OperationService operationService;

    public void runConsole() {

        final String email = request("Введите email");
        final String password = request("Введите password");

        final Menu regMenu = new Menu();
        regMenu.addEntry(new MenuEntry("Войти в ЛК") {
            @Override
            public void run() {
                userDTO = authService.auth(email);
                regMenu.setExit(true);
            }
        });
        regMenu.addEntry(new MenuEntry("Создать нового пользователя") {
            @Override
            public void run() {
                String name = request("Введите имя");
                String lastName = request("Введите фамилию");
                userDTO = authService.registration(email, password, name, lastName);
                regMenu.setExit(true);
            }
        });
        regMenu.run();

        if (userDTO != null) {
            if (userDTO.getEmail() == null) {
                System.out.println("Something went wrong");
                return;
            }
            System.out.println("Привет " + userDTO.getName() + "!" + "\n" + "Что надо сделать?");

            final Menu menuLK = new Menu();
            menuLK.addEntry(new MenuEntry("Работа со счетами") {
                @Override
                public void run() {
                    final Menu menuAccounts = new Menu();
                    menuAccounts.addEntry(new MenuEntry("Показать счета") {
                        @Override
                        public void run() {
                            userDTO.setAccounts(accountService.viewAccounts(userDTO.getId()));
                            if (userDTO.getAccounts() != null) {
                                for (Account account : userDTO.getAccounts()) {
                                    System.out.println(account);
                                }
                            } else {
                                System.out.println("Счета отсутствуют");
                            }
                        }
                    });
                    menuAccounts.addEntry(new MenuEntry("Добавить операцию") {
                        @Override
                        public void run() {
                            if (accountService.viewAccounts(userDTO.getId()) != null) {
                                for (Account account : accountService.viewAccounts(userDTO.getId())) {
                                    System.out.println(account);
                                }
                            } else {
                                System.out.println("Счета отсутствуют");
                            }

                            String temp;
                            Long idAccountSender = null;
                            temp = request("Укажите номер счета списания");
                            if (!isEmpty(temp)) {
                                idAccountSender = Long.parseLong(temp.trim());
                            }

                            Long idAccountAccepter = null;
                            temp = request("Укажите счет назначения \n (если нет, то просто нажмите Enter)");
                            if (!isEmpty(temp)) {
                                idAccountAccepter = Long.parseLong(temp.trim());
                            }

                            long sum = Long.parseLong(request("Укажите сумму"));
                            String date = null;

                            while (date == null || !date.matches("^(?:(?:31([-])(?:0?[13578]|1[02]))\\1|(?:(?:29|30)([-])(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29([-])0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])([-])(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$")
                            ) {
                                date = request("Укажите дату в формате ДД-ММ-ГГГГ \n (for TODAY just press ENTER)");
                                if (isEmpty(date)) {
                                    date = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
                                    System.out.println("Установлена дата: " + date);
                                }
                            }
                            String comment = request("Добавьте комментарий, или можете оставить пустым");
                            try {
                                final OperationModel operationDTO = operationService.addNewOperation(userDTO.getId(), idAccountSender, idAccountAccepter, sum, date, comment, new ArrayList<>());
                                System.out.println(operationDTO);
                            } catch (ServiceException e) {
                                if (e.getMessage().equals("Error with adding transaction")) {
                                    System.out.println("Not enough money for this operation");
                                } else {
                                    System.out.println(e.getMessage());
                                }
                            }
                        }
                    });
                    menuAccounts.addEntry(new MenuEntry("Создать новый счёт") {
                        @Override
                        public void run() {
                            String nameAccount = request("Введите имя счета");
                            accountService.makeAccount(nameAccount, userDTO.getId());
                            for (Account model : accountService.viewAccounts(userDTO.getId())) {
                                System.out.println(model);
                            }

                        }
                    });
                    menuAccounts.addEntry(new MenuEntry("Удалить счёт") {
                        @Override
                        public void run() {
                            String num = request("Введите номер удаляемого счета");
                            accountService.delAccount(Long.parseLong(num), userDTO.getId());
                            for (Account model : accountService.viewAccounts(userDTO.getId())) {
                                System.out.println(model);
                            }
                        }
                    });
                    menuAccounts.run();
                }
            });

            menuLK.addEntry(new MenuEntry("Отчеты") {
                @Override
                public void run() {
                    final Menu menuOperation = new Menu();
                    menuOperation.addEntry(new MenuEntry("Выборка по категориям") {
                        @Override
                        public void run() {
                            String startDate = request("Укажите дату начала периода в формате DD-MM-YYYY");
                            String finishDate = request("Укажите дату конца периода в формате DD-MM-YYYY");
                            try {
                                List<OperationModel> list = operationService.viewByCategoryAndDate(userDTO.getId(), startDate, finishDate);
                                list.forEach(x -> System.out.println(
                                        "Сумма операций по категории " +
                                                x.getCategories().toString() +
                                                " = " +
                                                x.getSum() +
                                                ", " +
                                                "Количество транзакций: "));

                            } catch (ServiceException e) {
                                System.out.println(e.getMessage());
                            }

                        }
                    });

                    menuOperation.run();
                }
            });
            menuLK.addEntry(new MenuEntry("Работа с категориями") {
                @Override
                public void run() {
                    final Menu menuCTG = new Menu();
                    menuCTG.addEntry(new MenuEntry("Просмотреть категории") {
                        @Override
                        public void run() {
                            ctgSrvs.view().forEach(x -> System.out.println("id " + x.getId() + " " + x.getName()));
                        }
                    });
                    menuCTG.addEntry(new MenuEntry("Добавить категорию") {
                        @Override
                        public void run() {
                            ctgSrvs.addCategories(request("Введите имя категории"));
                            ctgSrvs.view().forEach(x -> System.out.println("id " + x.getId() + " " + x.getName()));

                        }
                    });
                    menuCTG.addEntry(new MenuEntry("Удалить категорию") {
                        @Override
                        public void run() {
                            ctgSrvs.delCategories(Long.parseLong(request("Введите id категории")));
                            ctgSrvs.view().forEach(x -> System.out.println("id " + x.getId() + " " + x.getName()));

                        }
                    });
                    menuCTG.addEntry(new MenuEntry("Переименовать категорию") {
                        @Override
                        public void run() {
                            long id = Long.parseLong(request("Введите id категории"));
                            String name = request("Введите новое название");
                            ctgSrvs.setCategories(id, name);
                            ctgSrvs.view().forEach(System.out::println);

                        }
                    });
                    menuCTG.run();
                }

            });
            menuLK.run();
        } else {
            System.out.println("Ошибка авторизации");

        }
    }

    static String request(String title) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(title);
        return scanner.nextLine();
    }

    private static boolean isEmpty(final String str) {
        return str == null || str.trim().isEmpty();
    }


}
