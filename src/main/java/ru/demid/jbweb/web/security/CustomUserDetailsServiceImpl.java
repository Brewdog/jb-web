package ru.demid.jbweb.web.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.demid.jbweb.web.entity.UserModel;
import ru.demid.jbweb.web.repository.UserRepository;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CustomUserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository repo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserModel user = repo.findByEmail(email);
        if (user == null){
            throw new UsernameNotFoundException("User with email: " + email + "not found");
        }
        return new CustomUserDetailsImpl(
                user.getId(),
                user.getEmail(),
                user.getPassword(),
                user.getUserRoles()
                    .stream()
                    .map(UserGrantedAuthorityImpl::new)
                    .collect(Collectors.toList())
        );
    }
}
