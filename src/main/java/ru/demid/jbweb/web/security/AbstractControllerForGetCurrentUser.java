package ru.demid.jbweb.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.demid.jbweb.web.service.AuthService;
import ru.demid.jbweb.web.service.UserDTO;


public abstract class AbstractControllerForGetCurrentUser {
    @Autowired
    private AuthService authService;

    public UserDTO currentUser() throws UsernameNotFoundException{
        Object user = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (user instanceof CustomUserDetailsImpl) {
            CustomUserDetailsImpl userDetails = (CustomUserDetailsImpl) user;
            return authService.getUserByID(userDetails.getId());
        }
        return null;
    }
}