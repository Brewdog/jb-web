package ru.demid.jbweb.web.security;

public enum UserRoles {
    USER,
    ADMIN,
    GUEST
}
