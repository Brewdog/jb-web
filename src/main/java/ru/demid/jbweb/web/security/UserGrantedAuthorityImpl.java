package ru.demid.jbweb.web.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
public class UserGrantedAuthorityImpl implements GrantedAuthority {
    private final static String PREFIX = "ROLE_";
    private final UserRoles userRole;

    @Override
    public String getAuthority() {
        return PREFIX + userRole.name();
    }
}
