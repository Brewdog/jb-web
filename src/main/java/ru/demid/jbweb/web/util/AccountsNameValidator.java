package ru.demid.jbweb.web.util;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.demid.jbweb.web.entity.Account;
import ru.demid.jbweb.web.repository.AccountRepository;

@Component
@RequiredArgsConstructor
public class AccountsNameValidator implements Validator {
    AccountRepository accountRepository;
    @Override
    public boolean supports(Class<?> aClass) {
        return Account.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Account account = (Account) o;
        if (accountRepository.findAccountByName(account.getName())){
            errors.reject("name", "Name already exist");
        }
    }
}
